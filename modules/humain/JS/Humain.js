function Humain() {
  // La classe hérite du fichier JS de la zone courante.
  if (szZoneCourante == "site") {
    HumainPublic.apply(this, arguments);
  } else if (szZoneCourante == "application" || szZoneCourante == "admin") {
    HumainAdmin.apply(this, arguments);
  }

  var oThis = this;

  /**
   * Document Ready
   * Tout ce qui est ajouté ici sera automatiquement appelé au chargement.
   *
   * @return {void}
   */
  this.vInit = function() {
    this.vAfficheFilAriane("<h1>Gestion des humains</h1>");

    //------------------------
    // Select de la ville.
    //------------------------

    // L'élément du DOM représentant notre select.
    var eSelect = $("#zone_navigation_2 .select-ville");

    // On appelle la méthode permettant de transformer
    // notre select de villes.
    this.vTransformeSelectVille(eSelect);
  };
}
