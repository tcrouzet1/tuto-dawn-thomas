<?php

namespace APP\Modules\Humain\Models;

use APP\Modules\Base\Lib\Bdd as Bdd;

class Humain extends Bdd
{
    /**
     * Constructeur de la classe.
     *
     * @param  integer $nIdElement Id de l'élément.
     *
     * @return  void
     */
    public function __construct($nIdElement = 0)
    {
        parent::__construct();

        $this->aMappingChamps = array(
            'id_humain'                     => 'nIdHumain',
            'nom'                           => 'sNom',
            'prenom'                        => 'sPrenom',
            'date_naissance'                => 'dDateNaissance',
            'date_naissance_formate'        => 'dDateNaissanceFormate',
            'adresse'                       => 'sAdresse',
            'adresse_complement'            => 'sAdresseComplement',
            'id_commune'                    => 'nIdCommune',
            'code_postal_commune'           => 'sCodePostalCommune',
            'etat_sante'                    => 'sEtatSante',
            'armes'    => 'sArmes',
            'intelligence'                  => 'nIntelligence',
            'sens_tactique'                       => 'nTactique',
            'precision_h'                   => 'nPrecision',
            'intelligence_formate'          => 'sIntelligenceFormate',
            'sens_tactique_formate'               => 'sTactiqueFormate',
            'precision_formate'           => 'sPrecisionFormate',
            'passionne_violence'                   => 'nPassionneViolence',
            'sociable'                         => 'nSociable',
            'passionne_violence_formate'           => 'sPassionneViolenceFormate',
            'sociable_formate'                 => 'sSociableFormate',
            'latitude'                      => 'fLatitude',
            'longitude'                     => 'fLongitude',
        );

        $this->aTitreLibelle = array("nom", "prenom"); // affichage du nom et du prénom dans le titre

        if ($nIdElement > 0) {
            $aRecherche = array('nIdHumain' => $nIdElement);
            $aElements = $this->aGetElements($aRecherche);
            if (isset($aElements[0]) === true) {
                foreach ($aElements[0] as $szCle => $szValeur) {
                    $this->$szCle = $szValeur;
                }
            }
        }
    }


    /**
     * Requête de sélection.
     *
     * @param array     $aRecherche     Critères de recherche
     * @param string    $szOrderBy      Tri
     * @param boolean   $bModeCount     Juste compter.
     * @param integer   $nStart         Numéro de début.
     * @param integer   $nNbElements    Nombre de résultats.
     * @param string    $szOrderBy      Ordre de tri.
     * @param string    $szGroupBy      Groupé par tel champ.
     * @param string    $szContexte     Contexte d'appel de la requête.
     *
     * @return string                   Retourne la requête
     */
    public function szGetSelect($aRecherche = array(), $szOrderBy = '', $bModeCount = false, $nStart = 0, $nNbElements = '', $szGroupBy = '', $szContexte = '')
    {
        if ($szContexte == 'liste_ids') {
 
            // En mode liste d'ids, on ne renvoie qu'une
            // chaine contenant les ids séparés par des
            // virgules.
            $szChamps = "GROUP_CONCAT(HUM.id_humain SEPARATOR ', ') AS sIdsElements";
        } elseif ($bModeCount === false) {
            $szChamps = "
                HUM.id_humain AS nIdElement, HUM.id_humain, HUM.nom, HUM.prenom, HUM.date_naissance, 
                DATE_FORMAT(HUM.date_naissance, '%d/%m/%Y') AS date_naissance_formate
                , HUM.id_commune
                , HUM.armes
                , (
                    SELECT CONCAT(COM.code_postal, ' ', COM.commune) 
                    FROM commune COM
                    WHERE COM.id_commune = HUM.id_commune
                ) AS code_postal_commune
                , HUM.adresse
                , HUM.adresse_complement
                , HUM.latitude
                , HUM.longitude
                , HUM.etat_sante
                ,(
                    CASE
                        WHEN HUM.intelligence = 0
                        THEN 'Faible'
                        WHEN HUM.intelligence = 1
                        THEN 'Moyenne'
                        WHEN HUM.intelligence = 2
                        THEN 'Elevée'
                    END
                ) AS intelligence_formate,
                (
                    CASE
                        WHEN HUM.sens_tactique = 0
                        THEN 'Faible'
                        WHEN HUM.sens_tactique = 1
                        THEN 'Moyen'
                        WHEN HUM.sens_tactique = 2
                        THEN 'Elevé'
                    END
                ) AS sens_tactique_formate,
                (
                    CASE
                        WHEN HUM.precision_h = 0
                        THEN 'Faible'
                        WHEN HUM.precision_h = 1
                        THEN 'Moyenne'
                        WHEN HUM.precision_h = 2
                        THEN 'Elevée'
                    END
                ) AS precision_formate
                , HUM.passionne_violence
                , HUM.sociable
                , (
                    CASE
                        WHEN HUM.passionne_violence = 0
                        THEN 'Non'
                        WHEN HUM.passionne_violence = 1
                        THEN 'Oui'
                    END
                ) AS passionne_violence_formate
                , (
                    CASE
                        WHEN HUM.sociable = 0
                        THEN 'Non'
                        WHEN HUM.sociable = 1
                        THEN 'Oui'
                    END
                ) AS sociable_formate
            ";
        } else {
            $szChamps = "
                COUNT(*) AS nNbElements
            ";
        }
        $sRequete = "
            SELECT *
            FROM
            (
                SELECT " . $szChamps . "
                FROM humain AS HUM
                WHERE 1=1
        ";

        $sRequete .= $this->szGetCriteresRecherche($aRecherche);

        $sRequete .= "
            ) matable
        ";

        if ($szGroupBy != '') {
            $sRequete .= " GROUP BY " . $szGroupBy . " ";
        }

        if ($bModeCount === false && in_array($szContexte, array('liste_ids')) === false) {

            // Pas besoin de tri dans le cas d'un COUNT
            // ou de la récupération des ids via GROUP_CONCAT.

            if ($szOrderBy != '') {
                $sRequete .= "ORDER BY " . $szOrderBy;
            } else {
                $sRequete .= "ORDER BY nom ASC, prenom ASC";
            }
            // die($sRequete);
        }

        return $sRequete;
    }


    /**
     * Méthode permettant de compléter une requête avec des critères.
     *
     * @param array $aRecherche Critères de recherche
     *
     * @return string           Retourne le SQL des critères de recherche
     */
    protected function szGetCriteresRecherche($aRecherche = array())
    {
        $sRequete = '';
        if (isset($aRecherche['nIdHumain']) === true && $aRecherche['nIdHumain'] > 0) {
            $sRequete .= "
                AND HUM.id_humain = " . addslashes($aRecherche['nIdHumain']) . "
            ";
        }
        if (isset($aRecherche['sNom']) === true && $aRecherche['sNom'] != '') {
            $sRequete .= "
                AND HUM.nom LIKE '%" . addslashes($aRecherche['sNom']) . "%'
            ";
        }
        if (isset($aRecherche['sPrenom']) === true && $aRecherche['sPrenom'] != '') {
            $sRequete .= "
                AND HUM.prenom LIKE '%" . addslashes($aRecherche['sPrenom']) . "%'
            ";
        }

        if (isset($aRecherche['dDateNaissanceDebut']) === true && $aRecherche['dDateNaissanceDebut'] != '') {
            $sRequete .= "
                AND HUM.date_naissance >= '" . addslashes($this->sGetDateFormatUniversel($aRecherche['dDateNaissanceDebut'], 'Y-m-d')) . "'
            ";
        }
        if (isset($aRecherche['dDateNaissanceFin']) === true && $aRecherche['dDateNaissanceFin'] != '') {
            $sRequete .= "
                AND HUM.date_naissance <= '" . addslashes($this->sGetDateFormatUniversel($aRecherche['dDateNaissanceFin'], 'Y-m-d')) . "'
            ";
        }

        if (isset($aRecherche['nIdCommune']) === true && $aRecherche['nIdCommune'] != '') {
            $sRequete .= "
                AND HUM.id_commune = " . addslashes($aRecherche['nIdCommune']) . "
            ";
        }
        // Lorsqu'on passe un tableau d'ids, on le transforme en chaine
        // afin de l'inclure comme critère de recherche.
        if (isset($aRecherche['aIdsHumains']) === true && is_array($aRecherche['aIdsHumains']) === true) {
            $sRequete .= "AND HUM.id_humain IN('".implode("', '", $aRecherche['aIdsHumains'])."')";
        }
    
        return $sRequete;
    }
    
    /**
     * Insertion d'un élément.
     *
     * @return void
     */
    public function bInsert($aChamps = array())
    {
        $sRequete = '
        INSERT INTO humain
        SET ' . $this->sFormateChampsRequeteEdition($aChamps);
        //die($sRequete);
        $rLien = $this->rConnexion->query($sRequete);
        $this->nIdZHumain = $this->rConnexion->lastInsertId();

        if (!$rLien) {
            // Ecriture de la requête en échec dans les logs Apache.
            $this->vLogRequete($sRequete, true);
            $this->sMessagePDO = $this->rConnexion->sMessagePDO;
            return false;
        }

        return true;
    }
    /**
     * Permet de récupérer les critères de validation du formulaire d'édition.
     *
     * @param  string $szNomChamp Nom du champ.
     * @param  string $szType     Type de retour (chaine ou tableau).
     *
     * @return string             Critères (chaine ou tableau).
     */
    public function aGetCriteres($szNomChamp = '', $szType = 'tableau')
    {
        $aConfig['nIdHumain'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '11',
        );
        $aConfig['sNom'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '30',
        );
        $aConfig['sPrenom'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '30',
        );
        $aConfig['sEtatSante'] = array(
            'required' => '1',
        );
        $aConfig['nIdCommune'] = array(
            'required' => '1',
        );
        $aConfig['sAdresse'] = array(
            'required' => '1',
            'maxlength' => '255',
        );
        $aConfig['sAdresseComplement'] = array(
            'required' => '1',
            'maxlength' => '255',
        );
        $aConfig['dDateNaissance'] = array(
            'required' => '1',
            'maxlength' => '10',
        );
        $aConfig['fLatitude'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '20',
        );
        $aConfig['fLongitude'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '19',
        );

        if ($szType == 'tableau') {
            return $aConfig[$szNomChamp];
        } elseif ($szType == 'chaine') {
            if (isset($aConfig[$szNomChamp]) === true) {
                return $this->szGetCriteresValidation($aConfig[$szNomChamp]);
            }
        }
    }

    /**
     * Suppression d'un élément.
     *
     * @return void
     */
    public function bDelete()
    {
        $bRetour = false;

        $sRequete = '
        DELETE
        FROM humain
        WHERE id_humain = \'' . $this->nIdHumain . '\'
    ';

        $rLien = $this->rConnexion->query($sRequete);

        if ($rLien) {
            $bRetour = true;
        } else {
            $this->vLogRequete($sRequete, true);
            $this->sMessagePDO = $this->rConnexion->sMessagePDO;
        }

        return $bRetour;
    }

    /**
     * Mise à jour d'un élément.
     *
     * @return void
     */
    public function bUpdate($aChamps = array())
    {
        $bRetour = false;
        $aChampsNull = null;
        /*
    * Ici on prépare la partie de la requête contenant les placeholders (ex : '... nom = :nom ...')
    * et les champs dans un tableau
    */
        $aPreparationRequete = $this->aPreparerChampsPlaceHolderRequete($aChamps, $aChampsNull);

        //On concatène la partie placeholder avec le début de la requête et on ajoute la condition de mise à jour
        $sRequete = "UPDATE humain SET " . $aPreparationRequete['sRequete']
            . " WHERE id_humain = :nIdElement";

        //Ici la requête est préparée et on obtient un objet \PDOStatement : https://www.php.net/manual/fr/class.pdostatement.php
        $oRequetePrepare = $this->oPreparerRequete($sRequete);

        //On rajoute l'id élement dans les valeurs préparé pour qu'elle remplace le placeholder en plus des champs préparés auparavant
        $aPreparationRequete['aChampsPrepare'][':nIdElement'] = $this->nIdHumain;

        //Et finalement on exécute la requête
        $bRetour = $this->bExecuterRequetePrepare($oRequetePrepare, $aPreparationRequete['aChampsPrepare']);

        if ($bRetour === false) {
            $this->sMessagePDO = $this->rConnexion->sMessagePDO;
        }

        return $bRetour;
    }
 
    /**
     * Récupération d'un tableau d'ids correspondants à la recherche.
     *
     * @param  array  $aRecherche Critères de recherche.
     *
     * @return array              Liste des ids.
     */
    public function sGetIdsElements($aRecherche = array(), $sGroupBy = '')
    {
        // Pas besoin d'ORDER BY vu qu'on ne veut
        // que récupérer des ids.
        $szOrderBy = '';
 
        // On ne souhaite pas compter le nombre
        // de lignes.
        $bModeCount = false;
 
        // On ne souhaite pas récupérer une certaine
        // tranche d'éléments.
        $nStart = 0;
        $nNbElements = $_REQUEST['nNbElementsParPage'];
 
        // On indique bien le contexte qui fera qu'on
        // récupérera des ids et non pas les autres champs.
        $szContexte = 'liste_ids';
 
        // Récupération de la requête complète.
        $szRequete = $this->szGetSelect($aRecherche, $szOrderBy, $bModeCount, $nStart, $nNbElements, $sGroupBy, $szContexte);
 
        // Exécution de la requête.
        $aResultats = $this->aSelectBDD($szRequete, $this->aMappingChamps);
  
        $aRetour = array();
        if (isset($aResultats[0]) === true) {
 
            // On met tous les ids dans un tableau.
            $aRetour = explode(', ', $aResultats[0]->sIdsElements);
        }
  
        return $aRetour;
    }
}
