<?php

/**
 * 
 * On surcharge la méthode de connexion AuthUtilisateur::vConnexion 
 * pour prendre en compte le champ actif lors de la connexion. 
 * Si le champ actif est égal à 1 alors on laisse passer sinon non
 *
 */

namespace APP\Modules\Authentification\Models;

use APP\Modules\Authentification\Models\AuthUtilisateur as AuthUtilisateur;

class AuthUtilisateurSurcharge extends AuthUtilisateur
{

    /**
     * Connecte un utilisateur si le champ actif et égal à 1.
     *
     * @param  string   $szIdentifiant      Identifiant de connexion.
     * @param  string   $szMotDePasse       Mot de passe.
     * @param  integer  $nIdUtilisateur     Id de l'utilisateur.
     * @param  boolean  $bSubstitution      Substitution ou connexion simple.
     *
     * @return boolean                      Succès ou échec.
     */
    public function vConnexion($szIdentifiant = '', $szMotDePasse = '', $nIdUtilisateur = 0, $bSubstitution = false)
    {

        $bRetour = false;
        $nActif = 0;

        $sRequete = "SELECT actif FROM utilisateur WHERE identifiant = '" . addslashes($szIdentifiant) . "'";
        $aResultat = $this->aSelectBDD($sRequete);
        if ($aResultat) {

            $nActif = $aResultat[0]->actif;
        }

        if ($nActif == 1) {

            $bRetour = parent::vConnexion($szIdentifiant, $szMotDePasse, $nIdUtilisateur, $bSubstitution);

            // Si la connexion a réussi, alors on charge les idSections
            // que possède l'utilisateur
            if ($bRetour === true) {
                $this->bStockageIdSectionSession($szIdentifiant);
            }
        }

        return $bRetour;
    }

    /**
     * Stocke l'id des sections de l'utilisateur qui vient de se connecter
     * dans une variable de session
     * 
     * @param  string   $szIdentifiant      Identifiant de connexion.
     * @return boolean                      
     */
    public function bStockageIdSectionSession($szIdentifiant = '')
    {

        $aIdsSections = array();

        $sRequete = "
            SELECT section.id_section AS nIdSection
            FROM section
            LEFT JOIN zombie ON zombie.id_zombie = section.id_chef_section
            LEFT JOIN utilisateur ON utilisateur.id_utilisateur = zombie.id_utilisateur
            WHERE utilisateur.identifiant = '$szIdentifiant'
            ORDER BY section.date_creation ASC";
        $aResultat = $this->aSelectBDD($sRequete);

        if ($aResultat) {
            foreach ($aResultat as $aLigne) {
                array_push($aIdsSections, $aLigne->nIdSection);
            }
        }

        $_SESSION['aIdsSections'] = $aIdsSections;

        return true;
    }
}
