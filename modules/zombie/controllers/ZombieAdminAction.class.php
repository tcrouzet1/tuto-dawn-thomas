<?php

namespace APP\Modules\Zombie\Controllers;

use APP\Core\Lib\Interne\PHP\UndeadBrain as UndeadBrain;

class ZombieAdminAction extends UndeadBrain
{
    /**
     * Constructeur de la classe.
     *
     * @param  string  Action à effectuer.
     *
     * @return  void
     */
    public function __construct($szAction = '')
    {
        // On regarde si du contenu est disponible en cache.
        $szContenuEnCache = $this->szGetContenuEnCache();

        if ($szContenuEnCache != '') {

            // Si du contenu est disponible en cache, on le renvoie.
            echo $szContenuEnCache;
        } else {
            $nIdElement = 0;
            if (isset($_REQUEST['nIdElement']) === true) {
                $nIdElement = $_REQUEST['nIdElement'];
            }

            // Si aucun contenu n'est en cache, on traite l'action demandée.

            switch ($szAction) {

                case 'recherche':
                    $aRetour = $this->aRechercheElements();
                    break;

                case 'dynamisation_edition':
                    $aRetour = $this->aDynamisationEdition($nIdElement);
                    break;

                case 'enregistre_edition':
                    $aRetour = $this->aEnregistreEdition($nIdElement);
                    break;

                case 'dynamisation_consultation':
                    $aRetour = $this->aDynamisationConsultation($nIdElement);
                    break;

                case 'suppression':
                    $aRetour = $this->aSuppression($nIdElement);
                    break;

                case 'upload_avatar':
                    $aRetour = $this->aUploadAvatar($nIdElement);
                    break;

                case 'dynamisation_recherche':
                    $aRetour = $this->aDynamisationRecherche();
                    break;

                case 'export_donnees':
                    $aRetour = $this->vExport();
                    break;

                case 'dynamisation_recrutement_zombie':
                    $aRetour = $this->aDynamisationRecrutementZombie($nIdElement);
                    break;

                case 'enregistre_recrutement_zombie':
                    $aRetour = $this->aEnregistreRecrutementZombie($nIdElement);
                    break;
                    case 'dynamisation_trombinoscope_zombie':
                    $aRetour = $this->aDynamisationTrombinoscopeZombie();
                    break;
            }

            $szRetour = json_encode($aRetour);

            echo $szRetour;

            // Sauvegarde du contenu dans le cache.
            $this->vSauvegardeContenuEnCache($szRetour);
        }
    }


    /**
     * Recherche d'éléments.
     *
     * @return array Retour JSON.
     */
    private function aRechercheElements()
    {
        $aRetour = array(
            'aElements' => array(),
        );

        $oElement = $this->oNew('Zombie');

        // Critères de recherche filtrant la liste des éléments.
        $aRecherche = array();

        // Tous les paramètres dont le nom se termine par
        // Rch sont ajoutés comme critère dans la requête SQL.
        foreach ($_REQUEST as $sCle => $sValeur) {
            if (substr($sCle, -3) == 'Rch') {
                $aRecherche[str_replace('Rch', '', $sCle)] = $sValeur;
            }
        }

        // Nombre d'éléments par page à afficher dans la liste.
        // Par défaut, on prend le nombre paramétré par la conf.
        // Si on le reçoit en paramètre, la valeur devient prioritaire.
        $nNbElementsParPage = $GLOBALS['aParamsAppli']['aParamsListe']['nNbElementsParPage'];
        if (isset($_REQUEST['nNbElementsParPage']) === true && $_REQUEST['nNbElementsParPage'] > 0) {
            $nNbElementsParPage = $_REQUEST['nNbElementsParPage'];
        }

        // Contexte d'appel de la requête. Ici je vais préciser une
        // clé indiquant le contexte. Par exemple : une liste simple,
        // une liste détaillée. Cela dans le but de ne faire certaines
        // jointures uniquement dans des cas particuliers pour économiser
        // les ressources.
        $szContexte = '';

        // On peut passer une méthode de comptage particulière
        // en paramètre si on ne veut pas utiliser nGetNbElements.
        // Dans les faits, ce n'est jamais utilisé.
        $szGetNbElements = '';

        // Création et récupération des infos de pagination.
        $oPagination = new \StdClass();
        $oPagination = $this->oGetInfosPagination($oElement, $aRecherche, $nNbElementsParPage, $szGetNbElements, $szContexte);
        $aRetour['aPagination'] = $oPagination;

        // Numéro de départ de la tranche de sélection des éléments.
        // Par exemple, 20 éléments à partir du 30ème éléments.
        // On défini ici le 30.
        $nStart = $oPagination->nStart;

        // Tri d'affichage des éléments de la liste.
        // On le reçoit en paramètre.
        $szOrderBy = '';
        if (isset($_REQUEST['szOrderBy']) === true) {
            $szOrderBy = $_REQUEST['szOrderBy'];
        }

        // Groupement des éléments sur un champ en particulier.
        // Par exemple, un utilisateur a plusieurs adresses,
        // je veux l'utilisateur et une adresse. Je ne veux pas
        // n fois le même utilisateur. Je vais donc grouper par nom.
        $szGroupBy      = '';

        // Récupération des éléments.
        $aRetour['aElements'] = $oElement->aGetElements($aRecherche, $nStart, $nNbElementsParPage, $szOrderBy, $szGroupBy, $szContexte);

        // Récupération des ids des éléments trouvés.
        $aRetour['aIdsElement'] = $oElement->sGetIdsElements($aRecherche);

        return $aRetour;
    }

    /**
     * Dynamisation d'un élément.
     *
     * @param integer $nIdElement Id de l'élément.
     *
     * @return array Retour JSON.
     */
    protected function aDynamisationEdition($nIdElement = 0)
    {
        $aRetour = array(
            'oElement' => new \StdClass(),
        );

        if ($nIdElement > 0) {
            $oElement = $this->oNew('Zombie');
            $aRecherche = array(
                'nIdZombie' => $nIdElement
            );
            $aElements = $oElement->aGetElements($aRecherche);
            if (isset($aElements[0]) === true) {
                $aRetour['oElement'] = $aElements[0];
            }
        }

        // Récupération de la liste des membres
        // renseignés dans la conf du module.
        $aRetour['aMembres'] = $this->szGetParametreModule('zombie', 'aMembres');

        // Tous les selects à dynamiser doivent être déclarés dans
        // le "aSelects" du retour. Ici le "sEtatDecomposition"
        // représente la classe du select.
        // Le coeur se chargera d'insérer les éléments dans le select.
        $aRetour['aSelects'] = array(
            'sEtatDecomposition' => array()
        );

        // On récupère les valeurs renseignées dans le fichier de
        // conf du module.
        $aEtatDecomposition = $this->szGetParametreModule('zombie', 'aEtatsDecomposition');

        // On ajoute pour chaque valeur un couple "valeur" / "libelle"
        // afin que le coeur comprenne quelle valeur mettre en tant
        // que valeur de l'option et quelle valeur en tant que label.
        foreach ($aEtatDecomposition as $sCle => $sValeur) {
            $aRetour['aSelects']['sEtatDecomposition'][] = array(
                'valeur' => $sCle,
                'libelle' => $sValeur,
            );
        }

        return $aRetour;
    }

    /**
     * Enregistrement d'un élément.
     *
     * @param integer $nIdElement Id de l'élément.
     *
     * @return array Retour JSON.
     */
    private function aEnregistreEdition($nIdElement = 0)
    {
        $aRetour = array(
            'bSucces' => false,
            'bModif' => false,
        );

        $oElement = $this->oNew('Zombie', array($nIdElement));

        // La méthode sGetDateFormatUniversel() permet de convertir
        // une date de n'importe quel format en une date d'un
        // autre format.
        $aChamps = array(
            'nom' => $_REQUEST['sNom'],
            'prenom' => $_REQUEST['sPrenom'],
            'decomposition' => $_REQUEST['sEtatDecomposition'],
            'date_zombification' => $this->sGetDateFormatUniversel($_REQUEST['dDateZombification'], 'Y-m-d'),
            'membres' => implode('#', $_REQUEST['aMembres']), // On colle les membres en une chaine avec un # entre chaque
            'intelligence' => $_REQUEST['nIntelligence'],
            'vitesse' => $_REQUEST['nVitesse'],
            'agressivite' => $_REQUEST['nAgressivite'],
            'ami_humains' => $_REQUEST['nAmiHumains'],
            'vegan' => $_REQUEST['nVegan'],
            'id_commune' => $_REQUEST['nIdCommune'],
            'adresse' => $_REQUEST['sAdresse'],
            'latitude' => $_REQUEST['fLatitude'],
            'longitude' => $_REQUEST['fLongitude'],
        );

        if ($nIdElement > 0) {
            $aRetour['bModif'] = true;
            $aRetour['bSucces'] = $oElement->bUpdate($aChamps);
        } else {
            $aRetour['bSucces'] = $oElement->bInsert($aChamps);
        }

        $aRetour['nIdElement'] = $oElement->nIdZombie;

        return $aRetour;
    }
    /**
        * Suppression d'un élément.
        *
        * @param integer $nIdElement Id de l'élément.
        *
        * @return array Retour JSON.
        */
    private function aSuppression($nIdElement = 0)
    {
        $aRetour = array(
            'bSucces' => false,
            'szErreur' => '',
        );

        $oElement = $this->oNew('Zombie');
        $oElement->nIdZombie = $nIdElement;
        $aRetour['bSucces'] = $oElement->bDelete();

        if ($aRetour['bSucces'] === false) {
            $aRetour['szErreur'] = $oElement->sMessagePDO;
        } else {
            $aRetour['nIdElement'] = $nIdElement;
        }

        return $aRetour;
    }
    /**
     * Consultation d'un élément.
     *
     * @param integer $nIdElement Id de l'élément.
     *
     * @return array Retour JSON.
     */
    private function aDynamisationConsultation($nIdElement = 0)
    {
        $aRetour = array(
            'oElement' => new \StdClass(),
        );

        $oElement = $this->oNew('Zombie');
        $aRecherche = array(
            'nIdZombie' => $nIdElement,
        );

        $aElements = $oElement->aGetElements($aRecherche);
        if (isset($aElements[0]) === true) {
            $aRetour['oElement'] = $aElements[0];

            $aAvatar = glob($_SERVER['DOCUMENT_ROOT'] . '/data/zombie/avatars/' . $nIdElement . '-*');

            if (is_array($aAvatar) === true && isset($aAvatar[0]) === true && $aAvatar[0] != '') {
                $aRetour['oElement']->sUrlAvatar = $GLOBALS['aParamsAppli']['url_base'] . 'document/zombie/avatars/' . $nIdElement . '/visualise-document.html';
            } else {
                // Pas d'avatar, on prend celui par défaut.
                $aRetour['oElement']->sUrlAvatar = $GLOBALS['aParamsAppli']['url_base'] . 'institution/img/avatar_defaut_zombie.jpg';
            }
            // Récupération de la liste des membres
            // renseignés dans la conf du module.
            $aRetour['aMembres'] = $this->szGetParametreModule('zombie', 'aMembres');

            // Récupération et stockage du label correspondant
            // à l'état de décomposition.
            $aEtatDecomposition = $this->szGetParametreModule('zombie', 'aEtatsDecomposition');
            $aRetour['oElement']->sEtatDecompositionFormate = 'Inconnue';
            if (isset($aEtatDecomposition[$aRetour['oElement']->sEtatDecomposition]) === true) {
                $aRetour['oElement']->sEtatDecompositionFormate = $aEtatDecomposition[$aRetour['oElement']->sEtatDecomposition];
            }
            $aRetour['nNbSections'] = count($_SESSION['aIdsSections']);
        }

        return $aRetour;
    }
   
    /**
     * Upload de l'avatar.
     *
     * @param  integer $nIdElement Id du zombie.
     *
     * @return array Retour JSON.
     */
    private function aUploadAvatar($nIdElement = 0)
    {
        $aRetour = array();

        $rFichier = $_FILES['file'];
        $szDossierDestination = $_SERVER['DOCUMENT_ROOT'] . '/data/zombie/avatars/';

        // Si le dossier n'existe pas, on le crée.
        if (is_dir($szDossierDestination) === false) {
            mkdir($szDossierDestination, 0777, true);
        }

        // Le fichier une fois uploadé commencera par ce préfixe suivi d'un tiret...
        $szPrefixeFichier = $nIdElement;
        // ... suivi par cet identifiant :
        $szIdentifiant = 'avatar';

        // Utilisation d'une méthode du coeur permettant d'uploader un fichier
        // en utilisant le md5 du fichier pour vérifier sa présence.
        $aRetour['bSucces'] = $this->aUploadDocumentAvecMd5File($rFichier, $szDossierDestination, $szPrefixeFichier, $szIdentifiant);

        if ($aRetour['bSucces'] === false) {
            $aRetour['szErreur'] = 'Une erreur est survenue lors de l\'upload';
        }

        $aRetour['nIdElement'] = $nIdElement;

        return $aRetour;
    }

    /**
     * Dynamisation de la recherche.
     *
     * @return array Retour JSON.
     */
    private function aDynamisationRecherche()
    {
        $aRetour = array();

        // Récupération de la liste des membres
        // renseignés dans la conf du module.
        $aRetour['aMembres'] = $this->szGetParametreModule('zombie', 'aMembres');

        return $aRetour;
    }

    /**
     * Export des éléments recherchés.
     *
     * @return void
     */
    private function vExport()
    {
        // On convertie la chaine des ids en un
        // véritable tableau.
        $aIds = json_decode($_REQUEST['sIdsSelection']);

        // On ajoute notre tableau en tant que critère
        // de recherche afin de ne sélectionner que
        // ces zombies pour export.
        $aRecherche = array();
        $aRecherche['aIdsZombies'] = $aIds;

        // On lance la recherche avec nos critères.
        $oElement = $this->oNew('Zombie');
        $aElements = $oElement->aGetElements($aRecherche);

        // On crée un fichier CSV temporaire que
        // nous allons alimenter.
        $rFichierTmp = tempnam('/tmp', 'csv');
        $rFichier = fopen($rFichierTmp, 'w');
        $sDelimiteur = ';';

        // On déclare les entêtes de colonnes.
        $aLigneEntete = array(
            'NOM', 'PRENOM'
        );
        // On ajoute la ligne dans le fichier.
        fputcsv($rFichier, $aLigneEntete, $sDelimiteur);

        foreach ($aElements as $nIndex => $oUnElement) {

            // Pour chaque zombie, on va ajouter une ligne
            // contenant le nom et le prénom.
            $aUneLigne = array();

            $aUneLigne[0] = $oUnElement->sNom;
            $aUneLigne[1] = $oUnElement->sPrenom;

            fputcsv($rFichier, $aUneLigne, $sDelimiteur);
        }

        // On envoie les bons entêtes pour que le fichier
        // se télécharge correctement.
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=export-zombies-" . date('YmdHis') . ".csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        // On ferme le fichier et on supprime le
        // fichier temporaire.
        fclose($rFichier);
        readfile($rFichierTmp);
        unlink($rFichierTmp);

        // On fait un exit car comme l'action n'est pas
        // destinée à retourner du JSON, on s'arrête là.
        exit;
    }


    /**
     * Dynamise la vue de selection de section lors du recrutement du zombie
     *
     * @param integer $nIdElement Id de l'élément.
     *
     * @return array Retour JSON.
     */
    private function aDynamisationRecrutementZombie($nIdElement = 0)
    {
        $aRetour = array(
            'oElement' => new \StdClass(),
        );

        if ($nIdElement > 0) {
            $oElement = $this->oNew('Zombie');
            $aRecherche = array(
                'nIdZombie' => $nIdElement
            );
            $aElements = $oElement->aGetElements($aRecherche);
            if (isset($aElements[0]) === true) {
                $aRetour['oElement'] = $aElements[0];
            }

            $aRetour['aIdsSections'] = $_SESSION['aIdsSections'];
        }

        return $aRetour;
    }

    /**
     * Recuperation de l'id de section dans la variable de session
     * pour le recrutement du zombie
     * @param integer $nIdElement Id de l'élément.
     *
     * @return array Retour JSON.
     */
    private function aEnregistreRecrutementZombie($nIdElement = 0)
    {
        $aRetour = array(
            'bSucces' => false,
            'bModif' => false,
        );

        $oZombie = $this->oNew('Zombie', array($nIdElement));

        $aRetour['oElement'] = $oZombie;

        if (count($_SESSION['aIdsSections']) == 1) {
            $nIdSection = $_SESSION['aIdsSections'][0];
        } else {
            $nIdSection = $_REQUEST['nIdSection'];
        }

        $aChamps = array(
            'id_section' => $nIdSection,
        );

        $aRetour['bSucces'] = $oZombie->bRecruteZombie($aChamps);

        return $aRetour;
    }
    /**
      * Dynamise la vue du trombinoscope des zombies.
      *
      * @param integer $nIdSection Id de la section sélectionnée dans le champ de recherche.
      *
      * @return array Retour JSON.
      */
    private function aDynamisationTrombinoscopeZombie()
    {
        $aRetour = array();
  
        // Permet de lancer la callback
        $aRetour['bSucces'] = true;
  
        // On recherche uniquement les zombies sélectionnés
        // et on ajoute le résultat de la recherche dans le tableau JSON
        foreach ($_REQUEST as $sCle => $sValeur) {
            if (substr($sCle, -3) == 'Rch') {
                $aRecherche[str_replace('Rch', '', $sCle)] = $sValeur;
            }
        }
        $oElement = $this->oNew('Zombie');
        $aElements = $oElement->aGetElements($aRecherche);
        // Ajoute l'URL de l'avatar pour chaque zombie de la liste
        foreach ($aElements as $oElement) {
            $nIdElement = $oElement->nIdElement;
            // Avatar
            $aAvatar = glob($_SERVER['DOCUMENT_ROOT'].'/data/zombie/avatars/'.$nIdElement.'-*');
            if (is_array($aAvatar) === true && isset($aAvatar[0]) === true && $aAvatar[0] != '') {
                $oElement->sUrlAvatar = $GLOBALS['aParamsAppli']['url_base'].'document/zombie/avatars/'.$nIdElement.'/visualise-document.html';
            } else {
                // Pas d'avatar, on prend celui par défaut.
                $oElement->sUrlAvatar = $GLOBALS['aParamsAppli']['url_base'].'institution/img/avatar_defaut_zombie.jpg';
            }
        }
        $aRetour['aElements'] = $aElements;
  
  
        return $aRetour;
    }
}
