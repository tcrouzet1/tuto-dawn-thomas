<?php
  
namespace APP\Modules\Zombie\Controllers;

use APP\Core\Lib\Interne\PHP\UndeadBrain as UndeadBrain;

require $_SERVER['DOCUMENT_ROOT'].'lib/externe/dist/mPDF/PHP/mPDF/mpdf.php';
  
class CronZombieAdminAction extends UndeadBrain
{
    /**
     * Constructeur de la classe.
     *
     * @param  string  Action à effectuer.
     *
     * @return  void
     */
    public function __construct($szAction = '')
    {
        switch ($szAction) {
  
            case 'trombinoscope':
                $aRetour = $this->vGenerationTrombinoscope();
                break;
        }
    }
  
      
    /**
     * Génération du trombinoscope.
     *
     * @return void
     */
    private function vGenerationTrombinoscope()
    {
        // On récupère tous les zombies dans la base.
        $oElement = $this->oNew('Zombie');
        $aElements = $oElement->aGetElements();
  
        // On crée l'objet PDF dans lequel on va ajouter
        // nos pages.
        $mpdf = new \Mpdf('utf-8', 'A4-L');
 
        // On ajoute une page de titre.
        $mpdf->WriteHTML('<div style="margin: 270px; padding-top: 180px;"><h1 style="font-size: 60px;">Trombinoscope</h1></div>');
 
        // Compteur pour savoir combien de zombie on a
        // déjà placé dans la ligne courante.
        $nNbZombiesDansLigne = 0;
 
        // Nombre maximum de zombies que l'on doit
        // afficher par ligne.
        $nNbZombiesMaxParLigne = 100;
 
        foreach ($aElements as $nIndex => $oElement) {
 
            // On boucle sur tous les zombies trouvés dans la base.
  
            if ($nNbZombiesDansLigne == 0) {
                // Si on est sur une nouvelle ligne, on
                // réinitialise notre variable tampon.
                $sHtmlZombies = '';
            }
  
            // On recherche l'avatar du zombie.
            $aPhotos = glob($_SERVER['DOCUMENT_ROOT'].'data/zombie/avatars/'.$oElement->nIdZombie.'-avatar_*');
 
            if (isset($aPhotos[0]) === true && $aPhotos[0] != '') {
 
                // Si on trouve une photo.
 
                $oElement->sPhoto = $aPhotos[0];
                 
                // On récupère les dimensions de l'image
                // et on calcule son ratio.
                list($nLargeurOriginale, $nHauteurOriginale) = getimagesize($oElement->sPhoto);
                $nRatio = $nLargeurOriginale / $nHauteurOriginale;
  
                if ($nHauteurOriginale > 170) {
                    // On redimensionne l'image si elle est trop grande.
                    $this->bRedimensionneImageAvecOrientation($oElement->sPhoto, $oElement->sPhoto, 0, '', 170);
                }
            } else {
 
                // Si on trouve une photo, on affiche celle par défaut.
                $oElement->sPhoto = $_SERVER['DOCUMENT_ROOT'].'institution/img/avatar_defaut_zombie.jpg';
            }
             
            // On ajout dans notre variable tampn le nom,
            // le prénom et la photo.
            $sHtmlZombies .= '<div style="width: 32.5%; float: left; font-size: 11px; border:1px solid gray; height: 160px; margin-bottom: 5px; margin-left: 5px;">'
                                .'<div style="width: 30%; float: left;">'
                                    .'<img src="'.$oElement->sPhoto.'" style="height: 170px"/>'
                                .'</div>'
                                .'<div style="float: right; padding-top:5px; padding-left:10px;">'
                                    .'<div><span style="font-weight: bold;">'.$oElement->sNom.' '.$oElement->sPrenom.'</span></div>'
                                .'</div>'
                            .'</div>';
             
            // On incrémente le nombre de zombies
            // affichés dans la ligne.
            $nNbZombiesDansLigne++;
  
            if ($nNbZombiesDansLigne == $nNbZombiesMaxParLigne || $nNbZombiesDansLigne == count($aElements)) {
 
                // Si on a atteint le nombre max de zombies
                // dans la ligne OU qu'on a inséré le dernier
                // zombie présent en base, on écrit notre ligne.
                $mpdf->WriteHTML($sHtmlZombies);
 
                // Et on réinitialise le compteur de la ligne.
                $nNbZombiesDansLigne = 0;
            }
        }
         
        // Si le dossier du trombinoscope n'existe
        // pas, on le crée.
        $sSousDossier = $_SERVER['DOCUMENT_ROOT'].'/data/zombie/trombinoscope';
        if (is_dir($sSousDossier) === false) {
            mkdir($sSousDossier, 0777, true);
        }
         
        // On génère le PDF.
        $mpdf->Output($sSousDossier.'/trombinoscope.pdf', 'F');
    }
}
