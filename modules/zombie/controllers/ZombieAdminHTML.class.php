<?php

namespace APP\Modules\Zombie\Controllers;                       // Définition du namespace de la classe

use APP\Core\Lib\Interne\PHP\AffichageHTML as AffichageHTML;    // Création d’un alias pour utiliser la classe d’affichage

class ZombieAdminHTML extends AffichageHTML
{
    /**
     * Récupère le contenu central de la page.
     *
     * @return string $szContenu Contenu HTML.
     */
    public function szGetContenuCentralHTML()
    {
        // Appel de la méthode parente, qui insère les ressources
        // JS et CSS à la volée dans le modèle.
        $sContenu = parent::szGetContenuCentralHTML();

        if ($this->bAccueilModule === true) {
            if (isset($_REQUEST['bMesSoldats']) && $_REQUEST['bMesSoldats'] === 'true') {
                // Récupération de notre vue.
                $sFichierContenu = $this->szGetFichierPourInclusion('modules', 'zombie/vues/liste_soldat.html');
            } else {
                // Récupération de notre vue.
                $sFichierContenu = $this->szGetFichierPourInclusion('modules', 'zombie/vues/liste_zombie.html');
            }
            // Récupération du contenu de notre vue au format QueryPath
            // nous permettant de manipuler le DOM de manière similaire
            // à jQuery.
            $oContenu = $this->oGetVue($sFichierContenu);

            // On stocke le contenu au format HTML.
            $sContenu = $oContenu->find('body')->html();

            // En mode accueil,
            // on insère le HTML de l'accueil directement dans le modèle.
            $this->objQpModele->find('#zone_navigation_3')->html($sContenu);

            // Récupération et insertion de la vue de recherche.
            $sFichierContenu = $this->szGetFichierPourInclusion('modules', 'zombie/vues/recherche_zombie.html');
            $oContenu = $this->oGetVue($sFichierContenu);


            if (isset($_REQUEST['bMesSoldats']) && $_REQUEST['bMesSoldats'] === 'true') {
                $oContenu->find('#bMesSoldatsRch')->val('1');
            } else {
                $oContenu->find('#bMesSoldatsRch')->val('0');
            }
            $sContenu = $oContenu->find('body')->html();
            $this->objQpModele->find('#zone_navigation_2')->html($sContenu);
        } else {
            // En mode vue simple, on retourne le HTML de la vue.
            return $sContenu;
        }
    }

    /**
     * Page par défaut.
     *
     * @return string $szContenu Contenu HTML
     */
    public function szGetPageDefautHTML()
    {
        $szContenu = $this->szGetContenuVue();

        $objQP = $this->oGetVue('', $szContenu);
        $szContenu = $objQP->find('body')->innerHTML();

        return $szContenu;
    }
}
