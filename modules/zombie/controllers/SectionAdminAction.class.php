<?php

namespace APP\Modules\Zombie\Controllers;

use APP\Core\Lib\Interne\PHP\UndeadBrain as UndeadBrain;

class SectionAdminAction extends UndeadBrain
{
    /**
     * Constructeur de la classe.
     *
     * @param  string  Action à effectuer.
     *
     * @return  void
     */
    public function __construct($szAction = '')
    {
        // On regarde si du contenu est disponible en cache.
        $szContenuEnCache = $this->szGetContenuEnCache();

        if ($szContenuEnCache != '') {

            // Si du contenu est disponible en cache, on le renvoie.
            echo $szContenuEnCache;
        } else {
            $nIdElement = 0;
            if (isset($_REQUEST['nIdElement']) === true) {
                $nIdElement = $_REQUEST['nIdElement'];
            }

            // Si aucun contenu n'est en cache, on traite l'action demandée.

            switch ($szAction) {

                case 'recherche':
                    $aRetour = $this->aRechercheElements();
                    break;

                case 'dynamisation_edition':
                    $aRetour = $this->aDynamisationEdition($nIdElement);
                    break;

                case 'enregistre_edition':
                    $aRetour = $this->aEnregistreEdition($nIdElement);
                    break;

                case 'dynamisation_consultation':
                    $aRetour = $this->aDynamisationConsultation($nIdElement);
                    break;

                case 'suppression':
                    $aRetour = $this->aSuppression($nIdElement);
                    break;
            }

            $szRetour = json_encode($aRetour);

            echo $szRetour;

            // Sauvegarde du contenu dans le cache.
            $this->vSauvegardeContenuEnCache($szRetour);
        }
    }


    /**
     * Recherche d'éléments.
     *
     * @return array Retour JSON.
     */
    private function aRechercheElements()
    {
        $aRetour = array(
            'aElements' => array(),
        );

        $oElement = $this->oNew('Section');

        // Critères de recherche filtrant la liste des éléments.
        $aRecherche     = array();

        // Tous les paramètres dont le nom se termine par
        // Rch sont ajoutés comme critère dans la requête SQL.
        foreach ($_REQUEST as $sCle => $sValeur) {
            if (substr($sCle, -3) == 'Rch') {
                $aRecherche[str_replace('Rch', '', $sCle)] = $sValeur;
            }
        }

        // Nombre d'éléments par page à afficher dans la liste.
        // Par défaut, on prend le nombre paramétré par la conf.
        // Si on le reçoit en paramètre, la valeur devient prioritaire.
        $nNbElementsParPage = $GLOBALS['aParamsAppli']['aParamsListe']['nNbElementsParPage'];
        if (isset($_REQUEST['nNbElementsParPage']) === true && $_REQUEST['nNbElementsParPage'] > 0) {
            $nNbElementsParPage = $_REQUEST['nNbElementsParPage'];
        }

        // Contexte d'appel de la requête. Ici je vais préciser une
        // clé indiquant le contexte. Par exemple : une liste simple,
        // une liste détaillée. Cela dans le but de ne faire certaines
        // jointures uniquement dans des cas particuliers pour économiser
        // les ressources.
        $szContexte     = 'liste';

        // On peut passer une méthode de comptage particulière
        // en paramètre si on ne veut pas utiliser nGetNbElements.
        // Dans les faits, ce n'est jamais utilisé.
        $szGetNbElements = '';

        // Création et récupération des infos de pagination.
        $oPagination = new \StdClass();
        $oPagination = $this->oGetInfosPagination($oElement, $aRecherche, $nNbElementsParPage, $szGetNbElements, $szContexte);
        $aRetour['aPagination'] = $oPagination;

        // Numéro de départ de la tranche de sélection des éléments.
        // Par exemple, 20 éléments à partir du 30ème éléments.
        // On défini ici le 30.
        $nStart         = $oPagination->nStart;

        // Tri d'affichage des éléments de la liste.
        // On le reçoit en paramètre.
        $szOrderBy = '';
        if (isset($_REQUEST['szOrderBy']) === true) {
            $szOrderBy      = $_REQUEST['szOrderBy'];
        }

        // Groupement des éléments sur un champ en particulier.
        // Par exemple, un utilisateur a plusieurs adresses,
        // je veux l'utilisateur et une adresse. Je ne veux pas
        // n fois le même utilisateur. Je vais donc grouper par nom.
        $szGroupBy      = '';

        // Récupération des éléments.
        $aRetour['aElements'] = $oElement->aGetElements($aRecherche, $nStart, $nNbElementsParPage, $szOrderBy, $szGroupBy, $szContexte);

        return $aRetour;
    }

    /**
     * Dynamisation d'un élément.
     *
     * @param integer $nIdElement Id de l'élément.
     *
     * @return array Retour JSON.
     */
    protected function aDynamisationEdition($nIdElement = 0)
    {
        $aRetour = array(
            'oElement' => new \StdClass(),
        );

        if ($nIdElement > 0) {
            $oElement = $this->oNew('Section');
            $aRecherche = array(
                'nIdSection' => $nIdElement
            );
            $nStart = 0;
            $nNbElementsParPage = 20;
            $szOrderBy = '';
            $szGroupBy = '';
            $szContexte = 'edition';
            $aElements = $oElement->aGetElements($aRecherche, $nStart, $nNbElementsParPage, $szOrderBy, $szGroupBy, $szContexte);
            if (isset($aElements[0]) === true) {
                $aRetour['oElement'] = $aElements[0];
            }
        }

        return $aRetour;
    }

    /**
     * Enregistrement d'un élément.
     *
     * @param integer $nIdElement Id de l'élément.
     *
     * @return array Retour JSON.
     */
    private function aEnregistreEdition($nIdElement = 0)
    {
        $aRetour = array(
            'bSucces' => false,
            'bModif' => false,
        );

        $oElement = $this->oNew('Section', array($nIdElement));

        $aChamps = array(
            'libelle' => $_REQUEST['sLibelle'],
            'id_chef_section' => $_REQUEST['nIdChefSection'],
        );

        if ($nIdElement > 0) {
            $aRetour['bModif'] = true;
            $aRetour['bSucces'] = $oElement->bUpdate($aChamps);
        } else {
            $aRetour['bSucces'] = $oElement->bInsert($aChamps);
        }

        $aRetour['nIdElement'] = $oElement->nIdSection;

        return $aRetour;
    }

    /**
     * Consultation d'un élément.
     *
     * @param integer $nIdElement Id de l'élément.
     *
     * @return array Retour JSON.
     */
    private function aDynamisationConsultation($nIdElement = 0)
    {
        $aRetour = array(
            'oElement' => new \StdClass(),
        );

        $oElement = $this->oNew('Section');
        $aRecherche = array(
            'nIdSection' => $nIdElement,
        );

        $nStart = 0;
        $nNbElementsParPage = 20;
        $szOrderBy = '';
        $szGroupBy = '';
        $szContexte = 'consultation';
        $aElements = $oElement->aGetElements($aRecherche, $nStart, $nNbElementsParPage, $szOrderBy, $szGroupBy, $szContexte);
        if (isset($aElements[0]) === true) {
            $aRetour['oElement'] = $aElements[0];
        }

        return $aRetour;
    }

    /**
     * Suppression d'un élément.
     *
     * @param integer $nIdElement Id de l'élément.
     *
     * @return array Retour JSON.
     */
    private function aSuppression($nIdElement = 0)
    {
        $aRetour = array(
            'bSucces' => false,
            'szErreur' => '',
        );

        $oElement = $this->oNew('Section', array($nIdElement));
        //$oElement->nIdSection = $nIdElement;
        $aRetour['bSucces'] = $oElement->bDelete();

        if ($aRetour['bSucces'] === false) {
            $aRetour['szErreur'] = $oElement->sMessagePDO;
        } else {
            $aRetour['nIdElement'] = $nIdElement;
        }

        return $aRetour;
    }
}
