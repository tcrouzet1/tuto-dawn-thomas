<?php

namespace APP\Modules\Zombie\Models;

use APP\Modules\Base\Lib\Bdd as Bdd;

class Section extends Bdd
{

    /**
     * Constructeur de la classe.
     *
     * @param  integer $nIdElement Id de l'élément.
     *
     * @return  void
     */
    public function __construct($nIdElement = 0)
    {
        parent::__construct();

        $this->aMappingChamps = array(
            'id_section'                    => 'nIdSection',
            'id_chef_section'               => 'nIdChefSection',
            'chef_section_nom_prenom'       => 'sChefSectionNomPrenom',
            'libelle'                       => 'sLibelle',
            'date_creation'                 => 'dDateCreation',
            'date_creation_formate'         => 'dDateCreationFormate',
            'identifiant_chef_section'      => 'sIdentifiantChefSection',
        );

        $this->aTitreLibelle = array('libelle');

        if ($nIdElement > 0) {
            $aRecherche = array('nIdSection' => $nIdElement);
            $aElements = $this->aGetElements($aRecherche);
            if (isset($aElements[0]) === true) {
                foreach ($aElements[0] as $szCle => $szValeur) {
                    $this->$szCle = $szValeur;
                }
            }
        }
    }


    /**
     * Requête de sélection.
     *
     * @param array     $aRecherche     Critères de recherche
     * @param string    $szOrderBy      Tri
     * @param boolean   $bModeCount     Juste compter.
     * @param integer   $nStart         Numéro de début.
     * @param integer   $nNbElements    Nombre de résultats.
     * @param string    $szOrderBy      Ordre de tri.
     * @param string    $szGroupBy      Groupé par tel champ.
     * @param string    $szContexte     Contexte d'appel de la requête.
     *
     * @return string                   Retourne la requête
     */
    public function szGetSelect($aRecherche = array(), $szOrderBy = '', $bModeCount = false, $nStart = 0, $nNbElements = '', $szGroupBy = '', $szContexte = '')
    {
        if ($bModeCount === false) {
            $szChamps = "
                SEC.id_section AS nIdElement,
                SEC.id_section,
                SEC.id_chef_section,
                SEC.libelle,
                SEC.date_creation,
                DATE_FORMAT(SEC.date_creation, '%d/%m/%Y') AS date_creation_formate";


            if ($szContexte == 'liste' || $szContexte == 'edition' || $szContexte == 'consultation') {
                $szChamps .= ",(
                        SELECT CONCAT(ZOM.nom, ' ', ZOM.prenom)
                        FROM zombie ZOM
                        WHERE SEC.id_chef_section = ZOM.id_zombie
                    ) AS chef_section_nom_prenom
                    ";
            }
            if ($szContexte == 'consultation') {
                $szChamps .= ",(
                        SELECT identifiant
                        FROM utilisateur UTI
                        LEFT JOIN zombie ZOM ON ZOM.id_utilisateur = UTI.id_utilisateur
                       WHERE SEC.id_chef_section = ZOM.id_zombie
                      
                    ) AS identifiant_chef_section";
            }
        } else {
            $szChamps = '
                COUNT(*) AS nNbElements
            ';
        }

        $sRequete = '
            SELECT *
            FROM
            (
                SELECT ' . $szChamps . '
                FROM section AS SEC
                WHERE 1=1 
        ';

        $sRequete .= $this->szGetCriteresRecherche($aRecherche);
        if ($bModeCount === true && $szGroupBy != '') {
            $sRequete .= " GROUP BY ".$szGroupBy;
        }
        $sRequete .= '
            ) matable
        ';

        if ($szGroupBy != '' && $szContexte != 'calcul') {
            $sRequete .= " GROUP BY " . $szGroupBy . " ";
        }

        if ($bModeCount === false) {

            // Pas besoin de tri dans le cas d'un COUNT
            // ou de la récupération des ids via GROUP_CONCAT.

            if ($szOrderBy != '') {
                $sRequete .= "ORDER BY " . $szOrderBy;
            } else {
                $sRequete .= "ORDER BY libelle ASC ";
            }
            // die($sRequete);
        }

        return $sRequete;
    }

    /**
     * Méthode permettant de compléter une requête avec des critères.
     *
     * @param array $aRecherche Critères de recherche
     *
     * @return string           Retourne le SQL des critères de recherche
     */
    protected function szGetCriteresRecherche($aRecherche = array())
    {
        $sRequete = '';

        if (isset($aRecherche['nIdSection']) === true && $aRecherche['nIdSection'] > 0) {
            $sRequete .= '
                AND SEC.id_section = ' . addslashes($aRecherche['nIdSection']) . '
            ';
        }

        if (isset($aRecherche['sLibelle']) === true && $aRecherche['sLibelle'] != '') {
            $sRequete .= '
                AND SEC.libelle LIKE \'%' . addslashes($aRecherche['sLibelle']) . '%\'
            ';
        }

        if (isset($aRecherche['nIdChefSection']) === true && $aRecherche['nIdChefSection'] != '') {
            $sRequete .= "
                AND SEC.id_chef_section = " . addslashes($aRecherche['nIdChefSection']) . "
            ";
        }

        if (isset($aRecherche['dDateCreationDebut']) === true && $aRecherche['dDateCreationDebut'] != '') {
            // La méthode sGetDateFormatUniversel() permet de convertir
            // une date de n'importe quel format en une date d'un
            // autre format.
            $sRequete .= "
                AND SEC.date_creation >= '" . addslashes($this->sGetDateFormatUniversel($aRecherche['dDateCreationDebut'], 'Y-m-d')) . "'
            ";
        }

        if (isset($aRecherche['dDateCreationFin']) === true && $aRecherche['dDateCreationFin'] != '') {
            // La méthode sGetDateFormatUniversel() permet de convertir
            // une date de n'importe quel format en une date d'un
            // autre format.
            $sRequete .= "
                AND SEC.date_creation <= '" . addslashes($this->sGetDateFormatUniversel($aRecherche['dDateCreationFin'], 'Y-m-d')) . "'
            ";
        }

        return $sRequete;
    }

    /**
     * Permet de récupérer les critères de validation du formulaire d'édition.
     *
     * @param  string $szNomChamp Nom du champ.
     * @param  string $szType     Type de retour (chaine ou tableau).
     *
     * @return string             Critères (chaine ou tableau).
     */
    public function aGetCriteres($szNomChamp = '', $szType = 'tableau')
    {
        $aConfig['nIdSection'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '11',
        );

        $aConfig['sLibelle'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '30',
        );

        $aConfig['nIdChefSection'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '11',
        );

        if ($szType == 'tableau') {
            return $aConfig[$szNomChamp];
        } elseif ($szType == 'chaine') {
            if (isset($aConfig[$szNomChamp]) === true) {
                return $this->szGetCriteresValidation($aConfig[$szNomChamp]);
            }
        }
    }

    /**
     * Insertion d'un élément.
     *
     * @return void
     */
    public function bInsert($aChamps = array())
    {
        $sRequete = '
            INSERT INTO section
            SET ' . $this->sFormateChampsRequeteEdition($aChamps);

        $rLien = $this->rConnexion->query($sRequete);
        $this->nIdSection = $this->rConnexion->lastInsertId();

        if (!$rLien) {
            // Ecriture de la requête en échec dans les logs Apache.
            $this->vLogRequete($sRequete, true);
            $this->sMessagePDO = $this->rConnexion->sMessagePDO;
            return false;
        } else {
            // On logue l'info dans la ressource Logs.
            // $this->bSetLog('insert_section', $this->nIdSection);
            //  $this->bInsertUtilisateur($aChamps['id_chef_section']);
            $this->bActiveUtilisateur($aChamps['id_chef_section']);
        }

        return true;
    }

    /**
     * Mise à jour d'un élément.
     *
     * @return void
     */
    public function bUpdate($aChamps = array())
    {
        $bRetour = false;

        $sRequete = '
            UPDATE section SET ' . $this->sFormateChampsRequeteEdition($aChamps) . '
            WHERE 1
            AND id_section = \'' . $this->nIdSection . '\'
        ';

        $rLien = $this->rConnexion->query($sRequete);

        if (!$rLien) {
            // Ecriture de la requête en échec dans les logs Apache.
            $this->vLogRequete($sRequete, true);
            $this->sMessagePDO = $this->rConnexion->sMessagePDO;
            return false;
        } else {
            // On logue l'info dans la ressource Logs.
            $this->bActiveUtilisateur($aChamps['id_chef_section']);
        }

        return true;
    }


    /**
     * Insertion d'un chef de section (utilisateur) s'il n'existe pas déjà
     *
     * @param  integer $nIdChefSection Identifiant du zombie chef de section
     *
     * @return void
     */
    public function bInsertUtilisateur($nIdChefSection)
    {

        // On récupère le zombie chef de section
        $oZombie = $this->oNew('Zombie', array($nIdChefSection));

        // Verification si l'id du zombie n'existe pas dans le champ utilisateur
        // Si il n'existe pas, on crée un utilisateur en ajoutant un id_utilisateur au zombie
        // Que l'on associe avec l'id du groupe des chefs de section dans la table utilisateur_groupe.

        if (!$oZombie->nIdUtilisateur) {

            // Récupération du nom et du prénom du zombie chef de section
            $sRequete = "SELECT nom, prenom FROM zombie WHERE id_zombie = $nIdChefSection";
            $rLien = $this->rConnexion->query($sRequete);
            if ($rLien) {
                $aResultat = $rLien->fetchAll();
                $sNom = $aResultat[0]['nom'];
                $sPrenom = $aResultat[0]['prenom'];
            }

            // Formattage de l'identifiant sous le modele: nom.prenom (minuscules, sans accent)
            $sNom = $this->szGetChaineNettoyeeURLRewrite($sNom);
            $sPrenom = $this->szGetChaineNettoyeeURLRewrite($sPrenom);
            // Concaténation de l'identifiant
            $sIdentifiant = $sNom . '.' . $sPrenom;

            // Génération d'un identifiant unique
            $sIdentifiant = $this->sGenereIdentifiantUnique($sIdentifiant);

            // Récupération des paramètres de connexion par défaut
            $aParamsConnexionDefaut['aParamsConnexionDefaut'] = $this->szGetParametreModule('zombie', 'aParamsConnexionDefaut');
            $sMotDePasse = $aParamsConnexionDefaut['aParamsConnexionDefaut']['mot_de_passe'];

            //Cryptage du mot de passe par défaut
            $sSalt = $aParamsConnexionDefaut['aParamsConnexionDefaut']['salt'];
            $sMotDePasseCrypte = crypt($sMotDePasse, $sSalt);

            // Insertion d'un nouvel utilisateur (chef de section)
            $sRequete = "
                INSERT INTO
                    utilisateur (identifiant, mot_de_passe, salt)
                VALUES ('$sIdentifiant', '$sMotDePasseCrypte', '$sSalt')
                ";
            $rLien = $this->rConnexion->query($sRequete);
            $nIdUtilisateur = $this->rConnexion->lastInsertId();


            // Ajout d'un id_utilisateur au zombie chef de section
            $sRequete = "UPDATE zombie SET id_utilisateur = $nIdUtilisateur WHERE id_zombie = $nIdChefSection";
            $rLien = $this->rConnexion->query($sRequete);

            // On récupère l'identifiant du groupe des chefs de section dans la configuration du module

            $aGroupes['aGroupes'] = $this->szGetParametreModule('zombie', 'aGroupes');
            $nIdGroupe = $aGroupes['aGroupes']['chef_section'];

            // On ajoute l'utilisateur dans le groupe chef de section
            $oGroupe = $this->oNew('Groupe', array($nIdGroupe));
            $oGroupe->bAjouteUtilisateurDansGroupe($nIdUtilisateur, $nIdGroupe);
        }

        return true;
    }
    /**
     * Si l'identifiant existe déja, assigne un identifiant incrémenté
     * sous la forme "mon.utilisateur1, mon.utilisateur2..."
     *
     * @param  string $sIdentifiant Identifiant du zombie chef de section
     *
     * @return string $sIdentifiantValide
     */
    public function sGenereIdentifiantUnique($sIdentifiant)
    {
        $sIdentifiantValide = $sIdentifiant;
        $nIncrement = 1;


        // incrémente l'identifiant si l'utilisateur éxiste déja
        while ($this->bChampUniqueDejaUtilise('utilisateur', 'identifiant', $sIdentifiantValide) === true) {
            $nIncrement = $nIncrement++;
            $sIdentifiantValide = $sIdentifiant . '.' . $nIncrement;
        }

        return $sIdentifiantValide;
    }
    /**
     * Suppression d'un élément.
     *
     * @return bool
     */
    public function bDelete()
    {
        $bRetour = false;

        // Désactivation SI l'utilisateur n'a pas d'autres sections.
        $nIdChefSection = $this->nIdChefSection;

        $aRecherche = array(
            'nIdChefSection' => $nIdChefSection,
        );
        
        //on calcul le nombre de section
        $nNombreSections = $this->nGetNbElements($aRecherche, 'SEC.id_chef_section', 'calcul');
        if ($nNombreSections == 1) {
            $this->bDesactiveUtilisateur($nIdChefSection);
        }

        // Suppression de la section
        $sRequete = '
            DELETE
            FROM section
            WHERE id_section = \'' . $this->nIdSection . '\'
        ';

        $rLien = $this->rConnexion->query($sRequete);

        if ($rLien) {
            $bRetour = true;
        } else {
            $this->vLogRequete($sRequete, true);
            $this->sMessagePDO = $this->rConnexion->sMessagePDO;
        }

        return true;
    }

    /**
     * Si l'utilisateur n'existe pas -> Insertion (insertion avec le champ actif à 1)
     * Si l'utilisateur existe -> Activation  (passage à 1)
     *
     * @param  integer $nIdChefSection Identifiant du zombie chef de section
     *
     * @return void
     */
    public function bActiveUtilisateur($nIdChefSection)
    {

        // Récupère le zombie chef de section
        $oZombie = $this->oNew('Zombie', array($nIdChefSection));

        // Verification si l'id du zombie n'existe pas dans le champ utilisateur
        // Si il n'existe pas, on crée un utilisateur en ajoutant un id_utilisateur au zombie
        // Que l'on associe avec l'id du groupe des chefs de section dans la table utilisateur_groupe.

        if (!$oZombie->nIdUtilisateur) {
            $sNom = $oZombie->sNom;
            $sPrenom = $oZombie->sPrenom;

            // Formattage de l'identifiant sous le modele: nom.prenom (minuscules, sans accent)
            $sNom = $this->szGetChaineNettoyeeURLRewrite($sNom);
            $sPrenom = $this->szGetChaineNettoyeeURLRewrite($sPrenom);
            // Concaténation de l'identifiant
            $sIdentifiant = $sNom . '.' . $sPrenom;

            // Génération d'un identifiant unique
            $sIdentifiant = $this->sGenereIdentifiantUnique($sIdentifiant);

            // Récupération des paramètres de connexion par défaut
            $aParamsConnexionDefaut['aParamsConnexionDefaut'] = $this->szGetParametreModule('zombie', 'aParamsConnexionDefaut');
            $sMotDePasse = $aParamsConnexionDefaut['aParamsConnexionDefaut']['mot_de_passe'];

            //Cryptage du mot de passe par défaut
            $sSalt = $aParamsConnexionDefaut['aParamsConnexionDefaut']['salt'];
            $sMotDePasseCrypte = crypt($sMotDePasse, $sSalt);

            // Insertion d'un nouvel utilisateur
            $sRequete = "
                INSERT INTO
                    utilisateur (identifiant, mot_de_passe, salt, actif)
                VALUES ('$sIdentifiant', '$sMotDePasseCrypte', '$sSalt', 1)";
            $rLien = $this->rConnexion->query($sRequete);
            $nIdUtilisateur = $this->rConnexion->lastInsertId();

            // Ajout d'un id_utilisateur au zombie chef de section
            $sRequete = "UPDATE zombie SET id_utilisateur = $nIdUtilisateur WHERE id_zombie = $nIdChefSection";
            $rLien = $this->rConnexion->query($sRequete);


            // On récupère l'identifiant du groupe des chefs de section dans la configuration du module
            $aGroupes['aGroupes'] = $this->szGetParametreModule('zombie', 'aGroupes');
            $nIdGroupe = $aGroupes['aGroupes']['chef_section'];

            // On ajoute l'utilisateur dans le groupe chef de section
            $oGroupe = $this->oNew('Groupe', array($nIdGroupe));
            $oGroupe->bAjouteUtilisateurDansGroupe($nIdUtilisateur, $nIdGroupe);
        }

        // Si le compte utilisateur du zombie existe déjà -> passage du champ actif à 1
        else {
            $nIdUtilisateur = $oZombie->nIdUtilisateur;
            $sRequete = "UPDATE utilisateur SET actif = '1' WHERE utilisateur.id_utilisateur = $nIdUtilisateur";
            $rLien = $this->rConnexion->query($sRequete);
        }

        return true;
    }

    /**
     * Insertion d'un nouvel utilisateur
     *
     * @param  integer $nIdSection Identifiant de la section
     *
     * @return void
     */
    public function bDesactiveUtilisateur($nIdChefSection)
    {

        //Passage du champ actif à 0 (inactif)
        $sRequete = "
            UPDATE utilisateur UTI
            LEFT JOIN zombie ZOM ON ZOM.id_utilisateur = UTI.id_utilisateur
            LEFT JOIN section SEC on SEC.id_chef_section = ZOM.id_zombie
            SET actif = 0 WHERE SEC.id_chef_section = $nIdChefSection";

        $rLien = $this->rConnexion->query($sRequete);
        return true;
    }
}
