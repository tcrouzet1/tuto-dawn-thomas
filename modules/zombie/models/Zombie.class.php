<?php

namespace APP\Modules\Zombie\Models;

use APP\Modules\Base\Lib\Bdd as Bdd;

class Zombie extends Bdd
{
    /**
     * Constructeur de la classe.
     *
     * @param  integer $nIdElement Id de l'élément.
     *
     * @return  void
     */
    public function __construct($nIdElement = 0)
    {
        parent::__construct();

        $this->aMappingChamps = array(
            'id_zombie'                     => 'nIdZombie',
            'id_section'                    => 'nIdSection',
            'id_utilisateur'                => 'nIdUtilisateur',
            'section'                       => 'sSection',
            'nom'                           => 'sNom',
            'prenom'                        => 'sPrenom',
            'date_zombification'            => 'dDateZombification',
            'date_zombification_formate'    => 'dDateZombificationFormate',
            'membres'                       => 'sMembres',
            'intelligence'                  => 'nIntelligence',
            'vitesse'                       => 'nVitesse',
            'agressivite'                   => 'nAgressivite',
            'intelligence_formate'          => 'sIntelligenceFormate',
            'vitesse_formate'               => 'sVitesseFormate',
            'agressivite_formate'           => 'sAgressiviteFormate',
            'ami_humains'                   => 'nAmiHumains',
            'vegan'                         => 'nVegan',
            'ami_humains_formate'           => 'sAmiHumainsFormate',
            'vegan_formate'                 => 'sVeganFormate',
            'id_commune'                    => 'nIdCommune',
            'code_postal_commune'           => 'sCodePostalCommune',
            'latitude'                      => 'fLatitude',
            'longitude'                     => 'fLongitude',
            'adresse'                       => 'sAdresse',
            'decomposition'                 => 'sEtatDecomposition',
        );

        $this->aTitreLibelle = array('nom', 'prenom'); // affichage du nom et du prénom dans le titre

        if ($nIdElement > 0) {
            $aRecherche = array('nIdZombie' => $nIdElement);
            $aElements = $this->aGetElements($aRecherche);
            if (isset($aElements[0]) === true) {
                foreach ($aElements[0] as $szCle => $szValeur) {
                    $this->$szCle = $szValeur;
                }
            }
        }
    }


    /**
     * Requête de sélection.
     *
     * @param array     $aRecherche     Critères de recherche
     * @param string    $szOrderBy      Tri
     * @param boolean   $bModeCount     Juste compter.
     * @param integer   $nStart         Numéro de début.
     * @param integer   $nNbElements    Nombre de résultats.
     * @param string    $szOrderBy      Ordre de tri.
     * @param string    $szGroupBy      Groupé par tel champ.
     * @param string    $szContexte     Contexte d'appel de la requête.
     *
     * @return string                   Retourne la requête
     */
    public function szGetSelect($aRecherche = array(), $szOrderBy = '', $bModeCount = false, $nStart = 0, $nNbElements = '', $szGroupBy = '', $szContexte = '')
    {
        if ($szContexte == 'liste_ids') {

            // En mode liste d'ids, on ne renvoie qu'une
            // chaine contenant les ids séparés par des
            // virgules.
            $szChamps = "GROUP_CONCAT(ZOM.id_zombie SEPARATOR ', ') AS sIdsElements";
        } elseif ($bModeCount === false) {
            $szChamps = "
                ZOM.id_zombie AS nIdElement,
                ZOM.id_zombie,
                ZOM.id_utilisateur,
                ZOM.id_section,
                (
                    SELECT libelle FROM section SEC
                    WHERE ZOM.id_section = SEC.id_section
                    )AS section,
                ZOM.nom,
                ZOM.prenom,
                ZOM.decomposition,
                ZOM.date_zombification,
                DATE_FORMAT(ZOM.date_zombification, '%d/%m/%Y') AS date_zombification_formate,
                ZOM.membres,
                ZOM.intelligence,
                ZOM.vitesse,
                ZOM.agressivite,
                (
                    CASE
                        WHEN ZOM.intelligence = 0
                        THEN 'Idiot'
                        WHEN ZOM.intelligence = 1
                        THEN 'Normal'
                        WHEN ZOM.intelligence = 2
                        THEN 'Rusé'
                    END
                ) AS intelligence_formate,
                (
                    CASE
                        WHEN ZOM.vitesse = 0
                        THEN 'Lent'
                        WHEN ZOM.vitesse = 1
                        THEN 'Normal'
                        WHEN ZOM.vitesse = 2
                        THEN 'Rapide'
                    END
                ) AS vitesse_formate,
                (
                    CASE
                        WHEN ZOM.agressivite = 0
                        THEN 'Faible'
                        WHEN ZOM.agressivite = 1
                        THEN 'Normal'
                        WHEN ZOM.agressivite = 2
                        THEN 'Elevée'
                    END
                ) AS agressivite_formate,
                ZOM.ami_humains,
                ZOM.vegan,
                (
                    CASE
                        WHEN ZOM.ami_humains = 0
                        THEN 'Non'
                        WHEN ZOM.ami_humains = 1
                        THEN 'Oui'
                    END
                ) AS ami_humains_formate,
                (
                    CASE
                        WHEN ZOM.vegan = 0
                        THEN 'Non'
                        WHEN ZOM.vegan = 1
                        THEN 'Oui'
                    END
                ) AS vegan_formate,
                ZOM.id_commune,
                (
                    SELECT CONCAT(COM.code_postal, ' ', COM.commune)
                    FROM commune COM
                    WHERE COM.id_commune = ZOM.id_commune
                ) AS code_postal_commune,
                ZOM.latitude,
                ZOM.longitude,
                ZOM.adresse
            ";
        } else {
            $szChamps = '
                COUNT(*) AS nNbElements
            ';
        }
        $sRequete = '
            SELECT *
            FROM
            (
                SELECT ' . $szChamps . '
                FROM zombie AS ZOM
               
        ';

        if (isset($aRecherche['bMesSoldats']) === true && $aRecherche['bMesSoldats'] ==1) {
            $sRequete .= "
                RIGHT JOIN section SEC ON SEC.id_section = ZOM.id_section
                    LEFT JOIN zombie ZOM2 ON ZOM2.id_zombie = SEC.id_chef_section
                    LEFT JOIN utilisateur UTI ON UTI.id_utilisateur = ZOM2.id_utilisateur
                ";
        }




        $sRequete .=' WHERE 1=1 ';
        $sRequete .= $this->szGetCriteresRecherche($aRecherche);
        $sRequete .= '
            ) matable
        ';
        if ($szGroupBy != '') {
            $sRequete .= " GROUP BY " . $szGroupBy . " ";
        }

        if ($bModeCount === false && in_array($szContexte, array('liste_ids')) === false) {

            // Pas besoin de tri dans le cas d'un COUNT
            // ou de la récupération des ids via GROUP_CONCAT.

            if ($szOrderBy != '') {
                $sRequete .= "ORDER BY " . $szOrderBy;
            } else {
                $sRequete .= "ORDER BY nom ASC, prenom ASC";
            }
        }

        return $sRequete;
    }

    /**
     * Méthode permettant de compléter une requête avec des critères.
     *
     * @param array $aRecherche Critères de recherche
     *
     * @return string           Retourne le SQL des critères de recherche
     */
    protected function szGetCriteresRecherche($aRecherche = array())
    {
        $sRequete = '';
        if (isset($aRecherche['nIdZombie']) === true && $aRecherche['nIdZombie'] > 0) {
            $sRequete .= '
                AND ZOM.id_zombie = ' . addslashes($aRecherche['nIdZombie']) . '
            ';
        }
        if (isset($aRecherche['bMesSoldats']) === true && $aRecherche['bMesSoldats'] == 1) {
            $sRequete .= "
                AND UTI.id_utilisateur = ".$_SESSION['nIdUtilisateur']
            ;
        }
        // if (isset($aRecherche['nIdUtilisateur']) === true && $aRecherche['nIdUtilisateur'] > 0) {
        //     $sRequete .= '
        //         AND ZOM.id_utilisateur = '.addslashes($aRecherche['nIdUtilisateur']).'
        //     ';
        // }
        if (isset($aRecherche['sNom']) === true && $aRecherche['sNom'] != '') {
            $sRequete .= '
                AND ZOM.nom LIKE \'%' . addslashes($aRecherche['sNom']) . '%\'
            ';
        }
        if (isset($aRecherche['sPrenom']) === true && $aRecherche['sPrenom'] != '') {
            $sRequete .= '
                AND ZOM.prenom LIKE \'%' . addslashes($aRecherche['sPrenom']) . '%\'
            ';
        }
        if (isset($aRecherche['dDateZombificationDebut']) === true && $aRecherche['dDateZombificationDebut'] != '') {
            // La méthode sGetDateFormatUniversel() permet de convertir
            // une date de n'importe quel format en une date d'un
            // autre format.
            $sRequete .= "
                AND ZOM.date_zombification >= '" . addslashes($this->sGetDateFormatUniversel($aRecherche['dDateZombificationDebut'], 'Y-m-d')) . "'
            ";
        }
        if (isset($aRecherche['dDateZombificationFin']) === true && $aRecherche['dDateZombificationFin'] != '') {
            // La méthode sGetDateFormatUniversel() permet de convertir
            // une date de n'importe quel format en une date d'un
            // autre format.
            $sRequete .= "
                AND ZOM.date_zombification <= '" . addslashes($this->sGetDateFormatUniversel($aRecherche['dDateZombificationFin'], 'Y-m-d')) . "'
            ";
        }
        if (isset($aRecherche['aMembres']) === true && is_array($aRecherche['aMembres']) === true && count($aRecherche['aMembres']) > 0) {

            // Si on recherche en sélectionnant des membres on cherche
            // chaque membre sélectionné, dans le champ de la base
            // stockant la liste des membres séparés par des #.
            $sRequete .= "
                AND ( 1
            ";
            foreach ($aRecherche['aMembres'] as $nIndex => $sCleMembre) {
                $sRequete .= "
                    AND (
                        ZOM.membres LIKE '" . addslashes($sCleMembre) . "'
                        OR ZOM.membres LIKE '" . addslashes($sCleMembre) . "#%'
                        OR ZOM.membres LIKE '%#" . addslashes($sCleMembre) . "'
                        OR ZOM.membres LIKE '%#" . addslashes($sCleMembre) . "#%'
                    )
                ";
            }
            $sRequete .= "
                )
            ";
        }
        if (isset($aRecherche['nIntelligence']) === true && $aRecherche['nIntelligence'] != '') {
            $sRequete .= "
                AND ZOM.intelligence = " . addslashes($aRecherche['nIntelligence']) . "
            ";
        }
        if (isset($aRecherche['nVitesse']) === true && $aRecherche['nVitesse'] != '') {
            $sRequete .= "
                AND ZOM.vitesse = " . addslashes($aRecherche['nVitesse']) . "
            ";
        }
        if (isset($aRecherche['nAgressivite']) === true && $aRecherche['nAgressivite'] != '') {
            $sRequete .= "
                AND ZOM.agressivite = " . addslashes($aRecherche['nAgressivite']) . "
            ";
        }
        if (isset($aRecherche['nAmiHumains']) === true && $aRecherche['nAmiHumains'] != '') {
            $sRequete .= "
                AND ZOM.ami_humains = " . addslashes($aRecherche['nAmiHumains']) . "
            ";
        }
        if (isset($aRecherche['nVegan']) === true && $aRecherche['nVegan'] != '') {
            $sRequete .= "
                AND ZOM.vegan = " . addslashes($aRecherche['nVegan']) . "
            ";
        }
        if (isset($aRecherche['nIdCommune']) === true && $aRecherche['nIdCommune'] != '') {
            $sRequete .= "
                AND ZOM.id_commune = " . addslashes($aRecherche['nIdCommune']) . "
            ";
        }
        // Lorsqu'on passe un tableau d'ids, on le transforme en chaine
        // afin de l'inclure comme critère de recherche.
        if (isset($aRecherche['aIdsZombies']) === true && is_array($aRecherche['aIdsZombies']) === true) {
            $sRequete .= "AND ZOM.id_zombie IN ('" . implode("', '", $aRecherche['aIdsZombies']) . "')";
        }

        return $sRequete;
    }

    /**
     * Permet de récupérer les critères de validation du formulaire d'édition.
     *
     * @param  string $szNomChamp Nom du champ.
     * @param  string $szType     Type de retour (chaine ou tableau).
     *
     * @return string             Critères (chaine ou tableau).
     */
    public function aGetCriteres($szNomChamp = '', $szType = 'tableau')
    {
        $aConfig['nIdZombie'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '11',
        );
        $aConfig['sNom'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '30',
        );
        $aConfig['sPrenom'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '30',
        );
        $aConfig['sEtatDecomposition'] = array(
            'required' => '1',
            'minlength' => '1',
        );
        $aConfig['nIdCommune'] = array(
            'required' => '1',
            'minlength' => '1',
        );
        $aConfig['fLatitude'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '20',
        );
        $aConfig['fLongitude'] = array(
            'required' => '1',
            'minlength' => '1',
            'maxlength' => '19',
        );
        $aConfig['sAdresse'] = array(
            'required' => '1',
            'maxlength' => '255',
        );

        if ($szType == 'tableau') {
            return $aConfig[$szNomChamp];
        } elseif ($szType == 'chaine') {
            if (isset($aConfig[$szNomChamp]) === true) {
                return $this->szGetCriteresValidation($aConfig[$szNomChamp]);
            }
        }
    }

    /**
     * Insertion d'un élément.
     *
     * @return void
     */
    public function bInsert($aChamps = array())
    {
        $sRequete = '
            INSERT INTO zombie
            SET ' . $this->sFormateChampsRequeteEdition($aChamps);

        $rLien = $this->rConnexion->query($sRequete);
        $this->nIdZombie = $this->rConnexion->lastInsertId();

        if (!$rLien) {
            // Ecriture de la requête en échec dans les logs Apache.
            $this->vLogRequete($sRequete, true);
            $this->sMessagePDO = $this->rConnexion->sMessagePDO;
            return false;
        } else {
            // On logue l'info dans la ressource Logs.
            $this->bSetLog('insert_zombie', $this->nIdZombie);
        }

        return true;
    }

    /**
     * Mise à jour d'un élément.
     *
     * @return void
     */
    public function bUpdate($aChamps = array())
    {
        $bRetour = false;

        $sRequete = '
            UPDATE zombie SET ' . $this->sFormateChampsRequeteEdition($aChamps) . '
            WHERE 1
            AND id_zombie = \'' . $this->nIdZombie . '\'
        ';

        $rLien = $this->rConnexion->query($sRequete);

        if (!$rLien) {
            // Ecriture de la requête en échec dans les logs Apache.
            $this->vLogRequete($sRequete, true);
            $this->sMessagePDO = $this->rConnexion->sMessagePDO;
            return false;
        } else {
            // On logue l'info dans la ressource Logs.
            $this->bSetLog('update_zombie', $this->nIdZombie);
        }

        return true;
    }

    /**
     * Suppression d'un élément.
     *
     * @return void
     */
    public function bDelete()
    {
        $sRequete = '
            DELETE
            FROM zombie
            WHERE id_zombie = \'' . $this->nIdZombie . '\'
        ';

        $rLien = $this->rConnexion->query($sRequete);

        if (!$rLien) {
            // Ecriture de la requête en échec dans les logs Apache.
            $this->vLogRequete($sRequete, true);
            $this->sMessagePDO = $this->rConnexion->sMessagePDO;
            return false;
        } else {
            // On logue l'info dans la ressource Logs.
            $this->bSetLog('delete_zombie', $this->nIdZombie);
        }

        return true;
    }

    /**
     * Récupération d'un tableau d'ids correspondants à la recherche.
     *
     * @param  array  $aRecherche Critères de recherche.
     *
     * @return array              Liste des ids.
     */
    public function sGetIdsElements($aRecherche = array(), $sGroupBy = '')
    {
        // Pas besoin d'ORDER BY vu qu'on ne veut
        // que récupérer des ids.
        $szOrderBy = '';

        // On ne souhaite pas compter le nombre
        // de lignes.
        $bModeCount = false;

        // On ne souhaite pas récupérer une certaine
        // tranche d'éléments.
        $nStart = 0;
        $nNbElements = $_REQUEST['nNbElementsParPage'];

        // On indique bien le contexte qui fera qu'on
        // récupérera des ids et non pas les autres champs.
        $szContexte = 'liste_ids';

        // Récupération de la requête complète.
        $szRequete = $this->szGetSelect($aRecherche, $szOrderBy, $bModeCount, $nStart, $nNbElements, $sGroupBy, $szContexte);

        // Exécution de la requête.
        $aResultats = $this->aSelectBDD($szRequete, $this->aMappingChamps);

        $aRetour = array();
        if (isset($aResultats[0]) === true) {

            // On met tous les ids dans un tableau.
            $aRetour = explode(', ', $aResultats[0]->sIdsElements);
        }

        return $aRetour;
    }

    /**
     * Met à jour le champ id_section d'un zombie
     *
     * @return void
     */
    public function bRecruteZombie($aChamps = array())
    {
        $bRetour = false;
     
        $sRequete = '
            UPDATE zombie SET '.$this->sFormateChampsRequeteEdition($aChamps).'
            WHERE 1
            AND id_zombie = \''.$this->nIdZombie.'\'
        ';
        
        $rLien = $this->rConnexion->query($sRequete);

        if (!$rLien) {
            // Ecriture de la requête en échec dans les logs Apache.
            $this->vLogRequete($sRequete, true);
            $this->sMessagePDO = $this->rConnexion->sMessagePDO;
            return false;
        } else {
            // On logue l'info dans la ressource Logs.
            $this->bSetLog('recrute_zombie', $this->nIdZombie);
        }
      
        return true;
    }
}
