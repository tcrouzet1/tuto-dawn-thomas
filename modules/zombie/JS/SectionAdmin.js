function SectionAdmin() {
  Recherche.apply(this, arguments);
  var oThis = this;

  /**
   * Chargement des listeners des champs de la recherche.
   *
   * @param object oReponseJSON   Infos JSON récupérées lors de l'appel Ajax.
   * @param object oParams        Paramètres passés avant l'appel Ajax.
   *
   * @return void
   */
  this.vCallbackListeElement = function(oReponseJSON, oParams) {
    this.vChargeEvenementsChampsRecherche("", oParams);
  };

  /**
   * Dynamisation de l'édition d'un élément.
   *
   * @param  {object} oReponseJSON Informations au format JSON.
   * @param  {object} oParams      Infos concernant l'action lancée.
   *
   * @return {void}
   */
  this.vDynamisationEditionJSON = function(oReponseJSON, oParams) {
    // Id de l'élément
    var nIdElement = 0;
    if (
      typeof oReponseJSON.oElement != "undefined" &&
      typeof oReponseJSON.oElement.nIdElement != "undefined"
    ) {
      nIdElement = oReponseJSON.oElement.nIdElement;
    }

    var sIdentifiantCalque = "modal_calque_edition_section";

    var oModal = new Modal(sIdentifiantCalque);

    // Stockage de l'élément du DOM représentant le formulaire
    // de notre calque afin de préciser ce que le coeur
    // doit dynamiser.
    oParams.eFormulaire = oModal.eModal.find("form");

    // Demande au coeur de dynamiser le formulaire avec les
    // informations récupérées grâce à notre action.
    this.vChargeFormulaireData(oReponseJSON, oParams);

    // On ajoute l'identifiant de l'élément sur le bouton
    // afin que l'action et la route aient cette info
    // pour la transmettre au contrôleur d'action.
    oModal.eModal
      .find(".action_zombie_btn_enregistre_edition_section")
      .addClass("variable_1_" + nIdElement);

    // L'élément du DOM représentant notre select.
    var eSelect = oModal.eModal.find(".select-zombie");

    if (nIdElement > 0) {
      // En modif, on insère à la volée une option
      // déjà sélectionnée afin d'afficher la valeur
      // sélectionnée lors de l'enregistrement précédent.
      var eClone = eSelect.find(".clone").clone();
      eClone.val(oReponseJSON.oElement.nIdChefSection);
      eClone.text(oReponseJSON.oElement.sChefSectionNomPrenom);
      eClone.prop("selected", true);
      eSelect.append(eClone);
    }

    // On appelle la méthode permettant de transformer
    // notre select de zombie.
    this.vTransformeSelectZombie(eSelect);

    // Ouverture et stockage de l'instance de calque.
    oModal.oOpenModal();
    aInstancesCalques[sIdentifiantCalque] = oModal;
  };

  /**
   * Transformation du select de zombie en Select2
   * permettant de rechercher via appel Ajax.
   *
   * @param  {element} eSelect Elément du DOM représentant le select visé.
   *
   * @return {void}
   */
  this.vTransformeSelectZombie = function(eSelect) {
    var aChamps = ["id_zombie", "CONCAT(nom, ' ', prenom)"];

    // Les paramètres du select.
    // Les lignes commentées sont des lignes par défaut dans
    // la méthode aGetSelect2JSONResearch et donc je les
    // laisse juste pour info.
    var oParamsSelect = {
      eSelect2: eSelect, // L'élément du DOM représentant notre select.
      nMinimumLength: 3, // Le nombre minimum de caractères saisis pour lancer la recherche.
      aChamps: aChamps, // On précise quel champ va être dans la value de l'option (1er), et lequel va être dans le label (2nd).
      sTable: "zombie", // La table visée dans la base.
      sOrderBy: "nom" // Champ sur lequel trié.
      //bAllowClear: true // On peut ajouter une croix pour supprimer la valeur sélectionnée.
      // sModuleRoute: 'base',                // Le module contenant la route de dynamisation.
      // sRoute: 'json_load_select_research', // La route de dynamisation.
      // sSousMode: '',                       // Un éventuel sous mode dans le cas d'une route personnalisée.
      // sRestriction: '',                    // On peut ajouter un bout de SQL pour compléter le WHERE
    };

    // On instancie le select pour qu'il devienne Select2
    // et accepte la recherche via appel Ajax.
    this.aGetSelect2JSONResearch(oParamsSelect);
  };

  /**
   * Fermeture du calque d'édition et rafraichissement de la liste.
   *
   * @param  {object} oReponseJSON Informations au format JSON.
   * @param  {object} oParams      Infos concernant l'action lancée.
   *
   * @return {void}
   */
  this.vFermeEditionEtRefreshListe = function(oReponseJSON, oParams) {
    if (oReponseJSON.bModif === true) {
      // Si l'id de l'élément est supérieur à 0
      // on est dans le cadre d'une mise à jour
      // du calque après modification d'une section.

      // Nous allons exécuter exactement la même
      // action que si nous avions cliqué sur le
      // bouton de consultation depuis la liste.
      // Nous allons juste le faire totalement
      // manuellement.

      // Les paramètres à passer à la route (l'id de l'élément).
      var oParamsConsultation = {
        aVariables: [oReponseJSON.nIdElement]
      };
      // Exécution de l'action présente dans le module zombie
      // en lui passant nos paramètres définis ci-dessus.
      this.vExecuteAction(
        "",
        "zombie",
        "btn_ouverture_consultation_section",
        oParamsConsultation
      );
    }

    // Rafraichissement de la liste.
    this.vChargeListe("", $(".liste_section"));

    // Fermeture du calque d'édition.
    vFermeCalque("calque_edition_section");
  };

  /**
   * Dynamisation de la consultation lors de l'ouverture du calque.
   *
   * @param object oReponseJSON   Infos JSON récupérées lors de l'appel Ajax.
   * @param  {object} oParams      Infos concernant l'action lancée.
   *
   * @return void
   */
  this.vDynamisationConsultationJSON = function(oReponseJSON, oParams) {
    var nIdElement = 0;
    if (typeof oReponseJSON.oElement != "undefined") {
      if (typeof oReponseJSON.oElement.nIdElement != "undefined") {
        nIdElement = oReponseJSON.oElement.nIdElement;
      }
    }

    // Classe du calque sur leq uel travailler.
    var szIdCalque = "modal_calque_consultation_section";

    // Création du clone de calque.
    // Si le calque (bloc) a été paramétré comme multicalques alors
    // une copie va être créée en concaténant l'id du calque et celui
    // de l'élément.
    var oModal = new Modal(szIdCalque, nIdElement, oReponseJSON);

    if (nIdElement > 0) {
      // Insertion du titre.
      if (typeof oReponseJSON.oElement.sTitreLibelle != "undefined") {
        oModal.eModal
          .find(".sTitreLibelle")
          .text(oReponseJSON.oElement.sTitreLibelle);
      }

      $.each(oReponseJSON.oElement, function(sNomChamp, sValeur) {
        // On insère la valeur dans les éléments correspondants.
        oModal.eModal.find("." + sNomChamp).html(sValeur);
      });
    }

    // Ajout de l'id de l'élément sur le bouton d'action afin de
    // le transmettre à la route au final.
    oModal.eModal.find(".btn_action").addClass("variable_1_" + nIdElement);
    // Petite astuce ici : l'évènement du bouton de suppression depuis une liste
    // est géré. Nous allons l'utiliser ici sans être dans une liste ^^
    // Il nous suffit d'ajouter
    oModal.eModal.find(".btn_supp").attr("id", "btn_suppression_" + nIdElement);

    // Chargement du listener du bouton de suppression depuis la liste.
    this.vChargeEvenementsBoutonsLigne();

    // Ouverture et stockage de l'instance de calque.
    oModal.oOpenModal();
    aInstancesCalques[oModal.sIdModal] = oModal;
  };

  /**
   * Dynamisation de l'ajout d'un utilisateur
   *
   * @param object oReponseJSON    Infos JSON récupérées lors de l'appel Ajax.
   * @param  {object} oParams      Infos concernant l'action lancée.
   *
   * @return void
   */
  this.vDynamisationAjoutUtilisateurJSON = function(oReponseJSON, oParams) {
    // Rafraîchissement de la liste des sections
    oThis.vFermeEditionEtRefreshListe(oReponseJSON, {});
  };

  /**
   * Rafraichissement de la liste.
   *
   * @param object oReponseJSON   Infos JSON récupérées lors de l'appel Ajax.
   * @param object oParams        Paramètres passés avant l'appel Ajax.
   *
   * @return void
   */
  this.vFermeConsultationEtRefreshListe = function(oReponseJSON, oParams) {
    this.vChargeListe("", $(".liste_section"));
    vFermeCalque("calque_consultation_section_" + oReponseJSON.nIdElement);
  };
}
