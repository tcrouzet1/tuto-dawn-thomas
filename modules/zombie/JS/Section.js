function Section() {
  // La classe hérite du fichier JS de la zone courante.

  if (szZoneCourante == "site") {
    SectionPublic.apply(this, arguments);
  } else if (szZoneCourante == "application" || szZoneCourante == "admin") {
    SectionAdmin.apply(this, arguments);
  }

  var oThis = this;

  /**
   * Document Ready
   * Tout ce qui est ajouté ici sera automatiquement appelé au chargement.
   *
   * @return {void}
   */
  this.vInit = function() {
    if (szControllerCourant == "SectionAdmin") {
      this.vAfficheFilAriane("<h1>Gestion des sections</h1>");

      //--------------------
      // Select des zombies
      //--------------------

      // L'élément du DOM représentant notre select.
      var eSelect = $("#zone_navigation_2 .select-zombie");

      // On appelle la méthode permettant de transformer
      // notre select des zombies.
      this.vTransformeSelectZombie(eSelect);
    } else if (szControllerCourant == "ZombieAdmin") {
      oZombie = new Zombie();
      oZombie.vInit();
    }
  };
}
