function Zombie() {
  // La classe hérite du fichier JS de la zone courante.
  if (szZoneCourante == "site") {
    ZombiePublic.apply(this, arguments);
  } else if (szZoneCourante == "application" || szZoneCourante == "admin") {
    ZombieAdmin.apply(this, arguments);
  }

  var oThis = this;
  /**
   * Document Ready
   * Tout ce qui est ajouté ici sera automatiquement appelé au chargement.
   *
   * @return {void}
   */
  this.vInit = function() {
    if (szControllerCourant == "ZombieAdmin") {
      this.vAfficheFilAriane("<h1>Gestion des zombies</h1>");

      //------------------------
      // Select des membres.
      //------------------------

      // Lorsqu'on arrive sur l'accueil du module
      // on exécute une action permettant de récupérer
      // les infos de dynamisation de la recherche
      // (ex : liste des membres).
      var oParams = {
        eFormulaire: $("#zone_navigation_2 form") // On cible précisément le formulaire.
      };
      this.vExecuteAction("", "zombie", "btn_dynamisation_recherche", oParams);

      //------------------------
      // Select de la ville.
      //------------------------

      // L'élément du DOM représentant notre select.
      var eSelect = $("#zone_navigation_2 .select-ville");

      // On appelle la méthode permettant de transformer
      // notre select de villes.
      this.vTransformeSelectVille(eSelect);
    } else if (szControllerCourant == "SectionAdmin") {
      oSection = new Section();
      oSection.vInit();
    }
  };
}
