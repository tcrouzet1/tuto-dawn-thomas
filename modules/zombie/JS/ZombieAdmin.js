function ZombieAdmin() {
  Recherche.apply(this, arguments);
  var oThis = this;

  /**
   * Dynamisation de l'édition d'un élément.
   *
   * @param  {object} oReponseJSON Informations au format JSON.
   * @param  {object} oParams      Infos concernant l'action lancée.
   *
   * @return {void}
   */
  this.vDynamisationEditionJSON = function(oReponseJSON, oParams) {
    // Id de l'élément à éditer.
    var nIdElement = 0;
    if (
      typeof oReponseJSON.oElement != "undefined" &&
      typeof oReponseJSON.oElement.nIdElement != "undefined"
    ) {
      nIdElement = oReponseJSON.oElement.nIdElement;
    }

    var sIdentifiantCalque = "modal_calque_edition_zombie";

    var oModal = new Modal(sIdentifiantCalque);

    // Stockage de l'élément du DOM représentant le formulaire
    // de notre calque afin de préciser ce que le coeur
    // doit dynamiser.
    oParams.eFormulaire = oModal.eModal.find("form");

    // Demande au coeur de dynamiser le formulaire avec les
    // informations récupérées grâce à notre action.
    this.vChargeFormulaireData(oReponseJSON, oParams);

    // Chargement de la dropzone.
    this.vChargeDropzone(oModal);

    // On ajoute l'identifiant de l'élément sur le bouton
    // afin que l'action et la route aient cette info
    // pour la transmettre au contrôleur d'action.
    oModal.eModal
      .find(".action_zombie_btn_enregistre_edition_zombie")
      .addClass("variable_1_" + nIdElement);

    //---------------------------------------------
    // Dynamisation des cases à cocher de membres.
    //---------------------------------------------

    // On transforme la chaine des membres de la base
    // en un tableau JS.
    var aMembresSelectionnes = [];
    if (oReponseJSON.oElement.sMembres != null) {
      aMembresSelectionnes = oReponseJSON.oElement.sMembres.split("#");
    }

    // On stocke l'élément du DOM contenant les cases à cocher.
    var eBlocCible = oModal.eModal.find(".bloc-membre-zombie");

    // On clone la ligne de modèle de la case à cocher.
    var eCloneTemp = eBlocCible.find(".clone").clone();
    // On supprime la classe clone.
    eCloneTemp.removeClass("clone");

    var aMembres = oReponseJSON.aMembres;
    Object.keys(aMembres).map(function(sCleMembre, nIndex) {
      // Si le membre a été sélectionné précédemment
      // alors il faut sélectionner l'élément courant.
      var bSelected = false;
      Object.keys(aMembresSelectionnes).map(function(nIndexBdd) {
        if (aMembresSelectionnes[nIndexBdd] == sCleMembre) {
          bSelected = true;
        }
      });

      // On travaille sur un nouveau clone propre.
      var eClone = eCloneTemp.clone();

      // On stocke le champ et le label pour
      // travailler proprement dessus.
      var eChamp = eClone.find('input[type="checkbox"]');
      var eLabel = eClone.find("label");

      // On crée un id en suffixant avec la
      // clé du membre.
      var sIdChamp = eChamp + "_" + sCleMembre;

      // On insère la clé en tant que valeur
      // du champ.
      eChamp.val(sCleMembre);
      // Et on change son id.
      eChamp.attr("id", sIdChamp);

      if (bSelected === true) {
        // L'élément est sélectionné en base
        // on le coche donc.
        eChamp.prop("checked", "checked");
      }

      // Ecriture du label.
      eLabel.text(aMembres[sCleMembre]);
      // Et insertion du for correspondant
      // à l'id du champ.
      eLabel.attr("for", sIdChamp);

      // On insère le clone finalisé.
      eBlocCible.append(eClone);
    });

    // L'élément du DOM représentant notre select.
    var eSelect = oModal.eModal.find(".select-ville");

    if (nIdElement > 0) {
      // En modif, on insère à la volée une option
      // déjà sélectionnée afin d'afficher la valeur
      // sélectionnée lors de l'enregistrement précédent.
      var eClone = eSelect.find(".clone").clone();
      eClone.val(oReponseJSON.oElement.nIdCommune);
      eClone.text(oReponseJSON.oElement.sCodePostalCommune);
      eClone.prop("selected", true);
      eSelect.append(eClone);
    }

    // On appelle la méthode permettant de transformer
    // notre select de villes.
    this.vTransformeSelectVille(eSelect);

    var oParamsBloc = {};

    var oGeolocalisation = new Geolocalisation();
    var oParamGeolocalisation = {
      bVilleAjax: true,
      eParent: oModal.eModal,
      oCallback: oThis.oGetFonctionCallback(
        oThis,
        oThis.vCallbackOuvertureModalGeolocalisation,
        oParamsBloc
      )
    };

    oGeolocalisation.vChargeEvenementsGeolocalisation(oParamGeolocalisation);

    // Ouverture et stockage de l'instance de calque.
    oModal.oOpenModal();
    aInstancesCalques[sIdentifiantCalque] = oModal;
  };

  /**
   * Fermeture du calque d'édition et rafraichissement de la liste.
   *
   * @param  {object} oReponseJSON Informations au format JSON.
   * @param  {object} oParams      Infos concernant l'action lancée.
   *
   * @return {void}
   */
  this.vFermeEditionEtRefreshListe = function(oReponseJSON, oParams) {
    if (oReponseJSON.bModif === true) {
      // Si l'id de l'élément est supérieur à 0
      // on est dans le cadre d'une mise à jour
      // du calque après modification d'un zombie.

      // Nous allons exécuter exactement la même
      // action que si nous avions cliqué sur le
      // bouton de consultation depuis la liste.
      // Nous allons juste le faire totalement
      // manuellement.

      // Les paramètres à passer à la route (l'id de l'élément).
      var oParamsConsultation = {
        aVariables: [oReponseJSON.nIdElement]
      };
      // Exécution de l'action présente dans le modulé zombie
      // en lui passant nos paramètres définis ci-dessus.
      this.vExecuteAction(
        "",
        "zombie",
        "btn_ouverture_consultation_zombie",
        oParamsConsultation
      );
    }

    // Rafraichissement de la liste.
    this.vChargeListe("", $(".liste_zombie"));

    // Fermeture du calque d'édition.
    vFermeCalque("calque_edition_zombie");
  };

  /**
   * Rafraichissement de la liste.
   *
   * @param object oReponseJSON   Infos JSON récupérées lors de l'appel Ajax.
   * @param object oParams        Paramètres passés avant l'appel Ajax.
   *
   * @return void
   */
  this.vFermeConsultationEtRefreshListe = function(oReponseJSON, oParams) {
    this.vChargeListe("", $(".liste_zombie"));
    vFermeCalque("calque_consultation_zombie_" + oReponseJSON.nIdElement);
  };

  /**
   * Dynamisation de la consultation lors de l'ouverture du calque.
   *
   * @param object oReponseJSON   Infos JSON récupérées lors de l'appel Ajax.
   * @param  {object} oParams      Infos concernant l'action lancée.
   *
   * @return void
   */
  this.vDynamisationConsultationJSON = function(oReponseJSON, oParams) {
    var nIdElement = 0;
    if (typeof oReponseJSON.oElement != "undefined") {
      if (typeof oReponseJSON.oElement.nIdElement != "undefined") {
        nIdElement = oReponseJSON.oElement.nIdElement;
      }
    }

    // Classe du calque sur leq uel travailler.
    var szIdCalque = "modal_calque_consultation_zombie";

    // Création du clone de calque.
    // Si le calque (bloc) a été paramétré comme multicalques alors
    // une copie va être créée en concaténant l'id du calque et celui
    // de l'élément.
    var oModal = new Modal(szIdCalque, nIdElement, oReponseJSON);

    if (nIdElement > 0) {
      // Insertion du titre.
      if (typeof oReponseJSON.oElement.sTitreLibelle != "undefined") {
        oModal.eModal
          .find(".sTitreLibelle")
          .text(oReponseJSON.oElement.sTitreLibelle);
      }

      $.each(oReponseJSON.oElement, function(sNomChamp, sValeur) {
        // On insère la valeur dans les éléments correspondants.
        oModal.eModal.find("." + sNomChamp).html(sValeur);
      });

      //----------------------------------------------------
      // Dynamisation de la liste des membres.
      //----------------------------------------------------

      // On transforme la chaine des membres de la base
      // en un tableau JS.
      var aMembresSelectionnes = [];
      if (oReponseJSON.oElement.sMembres != null) {
        aMembresSelectionnes = oReponseJSON.oElement.sMembres.split("#");
      }

      // On stocke l'élément du DOM contenant les cases à cocher.
      var eBlocCible = oModal.eModal.find(".bloc-membre-zombie");
      // On supprime tous les li qui ne sont pas le clone modèle
      // ceci dans le cas du rafraichissement de la consultation
      // après enregistrement de la modification.
      eBlocCible.find("li:not(.clone)").remove();

      // On clone la ligne de modèle de la case à cocher.
      var eCloneTemp = eBlocCible.find(".clone").clone();
      // On supprime la classe clone.
      eCloneTemp.removeClass("clone");

      // Liste des membres.
      var aMembres = oReponseJSON.aMembres;

      if (aMembresSelectionnes.length == 0) {
        // Si aucun membre n'a été sélectionné
        // alors on affiche un message.
        eBlocCible.parent("div").html('<p align="center">Aucun membre</p>');
      }

      // Si le membre a été sélectionné précédemment
      // alors on va l'écrire dans la consultation.
      Object.keys(aMembresSelectionnes).map(function(nIndexBdd) {
        // On travaille sur un clone propre.
        var eClone = eCloneTemp.clone();

        // On parcourt la liste des membres présents
        // dans la conf pour écrire le libellé plutôt
        // que la clé.
        Object.keys(aMembres).map(function(sCleMembre, nIndex) {
          if (aMembresSelectionnes[nIndexBdd] == sCleMembre) {
            eClone.text(aMembres[sCleMembre]);
          }
        });

        // On insère le li.
        eBlocCible.append(eClone);
      });

      oModal.eModal
        .find(".sUrlAvatar")
        .attr("src", oReponseJSON.oElement.sUrlAvatar);
    }

    // Ajout de l'id de l'élément sur le bouton d'action afin de
    // le transmettre à la route au final.
    oModal.eModal.find(".btn_action").addClass("variable_1_" + nIdElement);

    // Petite astuce ici : l'évènement du bouton de suppression depuis une liste
    // est géré. Nous allons l'utiliser ici sans être dans une liste ^^
    // Il nous suffit d'ajouter
    oModal.eModal.find(".btn_supp").attr("id", "btn_suppression_" + nIdElement);

    // Chargement du listener du bouton de suppression depuis la liste.
    this.vChargeEvenementsBoutonsLigne();

    // Dans un premier temps, on va demander au
    // coeur de récupérer le HTML de la liste des
    // logs.
    // On va aussi lui préciser une fonction callback
    // qui sera exécutée une fois le HTML récupéré et inséré.

    // La callback recevra un objet contenant les paramètres suivants.
    var oParamsCallback = {
      nIdZombie: nIdElement, // L'id du zombie.
      eModal: oModal.eModal // La fenêtre modale contenant le widget.
    };

    // Le chargement du widget recevra un objet contenant
    // les paramètres suivants :
    var oParamsBloc = {
      oInstance: oThis, // L'instance de notre classe courante.
      oMethode: oThis.vDynamiseWidgetListeLogs, // La fonction callback à exécuter une fois le HTML inséré.
      oParams: oParamsCallback, // Les paramètres à passer à la fonction callback.
      eBloc: oModal.eModal.find(".id_bloc_widget_liste_logs") // L'élément du DOM représentant le widget à charger.
    };

    // On demande au coeur de charger notre widget.
    var oBloc = new Bloc();
    oBloc.vChargementBlocManuel(oParamsBloc);

    //----------------------------------------------------
    // Dynamisation du bouton de recrutement du zombie
    //----------------------------------------------------
    var elBoutonRecrutement = $(".btn_ouverture_recrutement_zombie");
    if (oReponseJSON.oElement.nIdSection > 0) {
      // Cache le bouton
      elBoutonRecrutement.hide();
    } else {
      // Affiche le bouton
      elBoutonRecrutement.show();
      // Charge le listener sur le bouton de recrutement
      // pour afficher un message de confirmation
      if (oReponseJSON.nNbSections == 1) {
        elBoutonRecrutement.removeClass("btn_action");
        elBoutonRecrutement.unbind("click");
        elBoutonRecrutement.click(function() {
          var oParamsConfirmation = {};
          oParamsConfirmation.aVariables = [nIdElement];
          oParamsConfirmation.oBouton = elBoutonRecrutement;
          oParamsConfirmation.sLibelleBoutonContinuer = "Recruter";
          var oCallbackConfirmation = oThis.oGetFonctionCallback(
            elBoutonRecrutement,
            function() {
              oThis.vExecuteAction(
                "",
                "zombie",
                "btn_enregistre_recrutement_zombie",
                oParamsConfirmation
              );
            },
            oParamsConfirmation
          );
          oThis.vAlerteConfirmation(
            "Êtes-vous sûr de vouloir recruter ce zombie moisi ?",
            oCallbackConfirmation,
            "",
            "",
            "",
            oParamsConfirmation
          );
        });
      }
    }

    //----------------------------------------------------
    // Dynamisation du bouton de suppression du zombie
    //----------------------------------------------------
    var elBoutonSuppression = $(".action_zombie_btn_suppression_zombie");
    elBoutonSuppression.unbind("click");
    var sMessage = "";

    //Affichage d'un message personnalisé en fonction si le zombie fait partie d'une
    //section ou pas.
    if (oReponseJSON.oElement.nIdSection > 0) {
      var sMessage =
        "Ce zombie est membre d'une unité, êtes-vous sûr de vouloir le supprimer ?";
    } else {
      var sMessage = "Êtes-vous sûr de vouloir le supprimer ?";
    }
    elBoutonSuppression.click(function() {
      var oParamsSuppression = {};
      oParamsSuppression.aVariables = [nIdElement];
      oParamsSuppression.oBouton = elBoutonSuppression;
      oParamsSuppression.sLibelleBoutonContinuer = "Supprimer";
      var oCallbackSuppression = oThis.oGetFonctionCallback(
        elBoutonSuppression,
        function() {
          oThis.vExecuteAction(
            "",
            "zombie",
            "btn_suppression_zombie",
            oParamsSuppression
          );
        },
        oParamsSuppression
      );
      oThis.vAlerteConfirmation(
        sMessage,
        oCallbackSuppression,
        "",
        "",
        "",
        oParamsSuppression
      );
    });
    // Ouverture et stockage de l'instance de calque.
    oModal.oOpenModal();
    aInstancesCalques[oModal.sIdModal] = oModal;
  };

  /**
   * Chargement des listeners des champs de la recherche.
   *
   * @param object oReponseJSON   Infos JSON récupérées lors de l'appel Ajax.
   * @param object oParams        Paramètres passés avant l'appel Ajax.
   *
   * @return void
   */
  this.vCallbackListeElement = function(oReponseJSON, oParams) {
    this.vChargeEvenementsChampsRecherche("", oParams);
    if (typeof oReponseJSON.aIdsElement != "undefined") {
      // Si on a récupéré les ids des éléments, on les renseignent
      // dans le textarea caché pour les avoir lors de l'export.
      oParams.eListe
        .siblings("form")
        .find(".sIdsSelection")
        .text(JSON.stringify(oReponseJSON.aIdsElement));
    }
    $(".btn-export").off("click");
    $(".btn-export").on("click", function() {
      // Lors du clic sur le bouton d'export.

      // On récupère la route d'export.
      var sRoute = oThis.szGetRoute("", "zombie", "json_export_csv");

      // Et on l'insère en tant que destination du formulaire.
      $(this)
        .parents(".div_table")
        .find('form[name="formulaire_selection_zombies"]')
        .attr("action", sRoute);

      // Ensuite on soumet le formulaire.
      $(this)
        .parents(".div_table")
        .find('form[name="formulaire_selection_zombies"]')
        .submit();
    });
  };

  /**
   * Chargement de la dropzone.
   *
   * @param  {object} oModal Instance du calque.
   *
   * @return {void}
   */
  this.vChargeDropzone = function(oModal) {
    // Options de la dropzone.
    var oOptions = {
      url: "/admin/zombie/upload-avatar.json", // URL d'upload de l'image.
      autoDiscover: false, // Désactivation de la prise en charge auto de Dropzone.
      uploadMultiple: false, // Upload d'une seule image.
      maxFiles: 1, // Au max 1 fichier.
      autoProcessQueue: false, // On uploade pas directement après l'ajout du fichier.
      addRemoveLinks: true, // Affichage du bouton de suppression après upload.
      init: function() {
        // Récupération du bouton qui lancera l'upload.
        var eBoutonSubmit = oModal.eModal.find(".btn-submit-dropzone");

        // Stockage de l'objet de la dropzone.
        var oMaDropzone = this;

        /**
         * Callback d'ajout de fichier.
         * @param  {object} file Fichier.
         * @return {void}
         */
        this.on("addedfile", function(file) {
          $(oMaDropzone.element)
            .find(".dropzone-titre")
            .hide();
        });

        /**
         * Callback de suppression de fichier.
         * @param  {object} file Fichier.
         * @return {void}
         */
        this.on("removedfile", function(file) {
          $(oMaDropzone.element)
            .find(".dropzone-titre")
            .show();
        });

        // Listener sur le bouton
        eBoutonSubmit.off("click");
        eBoutonSubmit.on("click", function() {
          // On convertie le retour ajax stocké sur notre
          // bouton au format JSON.
          var oReponseJSON = JSON.parse(eBoutonSubmit.val());

          if (oMaDropzone.getQueuedFiles().length > 0) {
            // Si on a un élément à uploader, on upload.
            oMaDropzone.options.url =
              "/admin/zombie/" +
              oReponseJSON.nIdElement +
              "/upload-avatar.json";
            oMaDropzone.processQueue();
          } else {
            // Si on a plus de fichier à uploader,
            // on ferme le calque et on refresh la liste.
            oThis.vFermeEditionEtRefreshListe(oReponseJSON, {});
          }
        });

        /**
         * Callback après la fin de la queue de fichier.
         * @param  {object} file Fichier.
         * @return {void}
         */
        this.on("queuecomplete", function(file, res) {
          // On convertie le retour ajax stocké sur notre
          // bouton au format JSON.
          var oReponseJSON = JSON.parse(eBoutonSubmit.val());

          if (oMaDropzone.files[0].status != Dropzone.SUCCESS) {
            // Correctif pour un bug dropzone.js : https://github.com/enyo/dropzone/issues/578
            // Si le premier fichier est invalide, ne rien faire.
          } else {
            // Si on a plus de fichier à uploader,
            // on ferme le calque et on refresh la liste.
            oThis.vFermeEditionEtRefreshListe(oReponseJSON, {});
          }
        });
      }
    };
    oModal.eModal.find(".ma_dropzone").dropzone(oOptions);
  };

  /**
   * Upload de l'avatar si besoin, sinon, fermeture du calque
   * et rafraichissement de la liste.
   *
   * @param  {object} oReponseJSON Informations au format JSON.
   * @param  {object} oParams      Infos concernant l'action lancée.
   *
   * @return {void}
   */
  this.vUploadSiBesoin = function(oReponseJSON, oParams) {
    // On déduit le calque dans le calque on travaille
    // à partir du bouton sur lequel on vient de cliquer.
    var eCalque = $(oParams.oAction.oBouton).parents(".calque");

    // On stocke dans le bouton qui déclenche le traitement
    // de la dropzone le retour de notre appel ajax pour
    // conserver notre id de zombie et pour savoir si on
    // est en modif ou en ajout.
    // On convertie notre retour JSON en chaine de caractères.
    eCalque.find(".btn-submit-dropzone").val(JSON.stringify(oReponseJSON));

    // On déclenche le traitement de la dropzone.
    eCalque.find(".btn-submit-dropzone").trigger("click");
  };

  /**
   * Dynamisation du moteur de recherche en insérant les valeurs
   * dans les selects et dans les switchs.
   *
   * @param  {object} oReponseJSON Informations au format JSON.
   * @param  {object} oParams      Infos concernant l'action lancée.
   *
   * @return {void}
   */
  this.vDynamisationFormulaireRecherche = function(oReponseJSON, oParams) {
    // On attrape notre select dans le DOM.
    var eSelectMembres = oParams.eFormulaire.find(".select-membres");

    var aMembres = oReponseJSON.aMembres;

    // On instancie notre Select2 en lui passant notre
    // liste de membres en la formatant pour la rendre
    // compatible.
    eSelectMembres.select2({
      data: Object.keys(aMembres).map(function(sCleMembre, nIndex) {
        // Ici, on boucle sur les membres et pour chaque
        // ligne, on renvoie un objet composé d'une
        // propriété id et text.
        return {
          id: sCleMembre,
          text: aMembres[sCleMembre]
        };
      }),
      tags: true // On indique que l'on pourra sélectionner plusieurs valeurs.
    });

    // Lors de la sélection/désélection d'une valeur
    // on lance le chargement de la liste pour
    // rechercher.
    eSelectMembres.on("change", function() {
      oThis.vChargeListe("", $(".liste_zombie"));
    });
  };

  /**
   * Transformation du select de ville en Select2
   * permettant de rechercher via appel Ajax.
   *
   * @param  {element} eSelect Elément du DOM représentant le select visé.
   *
   * @return {void}
   */
  this.vTransformeSelectVille = function(eSelect) {
    var aChamps = ["id_commune", "CONCAT(code_postal, ' ', commune)"];

    // Les paramètres du select.
    // Les lignes commentées sont des lignes par défaut dans
    // la méthode aGetSelect2JSONResearch et donc je les
    // laisse juste pour info.
    var oParamsSelect = {
      eSelect2: eSelect, // L'élément du DOM représentant notre select.
      nMinimumLength: 3, // Le nombre minimum de caractères saisis pour lancer la recherche.
      aChamps: aChamps, // On précise quel champ va être dans la value de l'option (1er), et lequel va être dans le label (2nd).
      sTable: "commune", // La table visée dans la base.
      sOrderBy: "code_postal" // Champ sur lequel trié.
      // bAllowClear: false,                  // On peut ajouter une croix pour supprimer la valeur sélectionnée.
      // sModuleRoute: 'base',                // Le module contenant la route de dynamisation.
      // sRoute: 'json_load_select_research', // La route de dynamisation.
      // sSousMode: '',                       // Un éventuel sous mode dans le cas d'une route personnalisée.
      // sRestriction: '',                    // On peut ajouter un bout de SQL pour compléter le WHERE
    };

    // On instancie le select pour qu'il devienne Select2
    // et accepte la recherche via appel Ajax.
    this.aGetSelect2JSONResearch(oParamsSelect);
  };

  /**
   * Callback appelée juste après l'ouverture du
   * calque de géolocalisation d'une adresse.
   *
   * @param  {object} oParams Paramètres (bVilleAjax, eParent, oCallback).
   *
   * @return {void}
   */
  this.vCallbackOuvertureModalGeolocalisation = function(oParams) {
    // Calque de la géolocalisation d'une adresse.
    var eModal = $(".calque_geolocalisation_adresse");

    // Select ajax de la ville.
    var eSelect = oParams.eParent.find(".select-ville");

    var oCommune = oParams.oGeolocalisation.oDetailCommuneDepuisLibelle(
      eSelect
    );

    // Insertion de l'adresse dans le formulaire de géolocalisation.
    eModal.find(".sAdresse").val(oParams.eParent.find(".sAdresse").val());
    // Insertion du code postal dans le formulaire de géolocalisation.
    eModal.find(".sCodePostal").val(oCommune.sCodePostal);
    // Insertion de la ville dans le formulaire de géolocalisation.
    eModal.find(".sVille").val(oCommune.sVille);
    // Insertion d'une option sélectionnée par défaut
    // correspondant à la fille sélectionnée.
    eModal
      .find(".select-ville")
      .append(
        '<option value="' +
          oCommune.nIdCommune +
          '" selected>' +
          oCommune.sCodePostal +
          " " +
          oCommune.sVille +
          "</option>"
      );

    // On appelle la méthode permettant de transformer
    // notre select de villes.
    var eSelect = $(".calque_geolocalisation_adresse .select-ville");
    this.vTransformeSelectVille(eSelect);

    //----------------------------------------------
    // Lors de la sélection d'une commune, on
    // éclate son libellé et son code postal pour
    // les stocker dans les champs text cachés.
    //----------------------------------------------
    eModal.find(".select-ville").on("select2:select", function(e) {
      oCommune = oParams.oGeolocalisation.oDetailCommuneDepuisLibelle($(this));
      // Insertion du code postal dans le formulaire de géolocalisation.
      eModal.find(".sCodePostal").val(oCommune.sCodePostal);
      // Insertion de la ville dans le formulaire de géolocalisation.
      eModal.find(".sVille").val(oCommune.sVille);
    });

    // On déclenche la géolocalisation.
    var oParamsCallback = {
      eParent: oParams.eParent,
      eModal: eModal,
      oGeolocalisation: oParams.oGeolocalisation
    };
    var oParamsGeolocalisation = {
      oParamsCallbackCustom: oParamsCallback,
      oCallbackCustom: oThis.oGetFonctionCallback(
        oThis,
        oThis.vChargeEvenementClicEnregistrementGeolocalisation
      )
    };
    this.vExecuteAction(
      "",
      "geolocalisation",
      "btn_geolocalisation_adresse",
      oParamsGeolocalisation
    );
  };

  /**
   * Callback appelée juste après la fin de la
   * géolocalisation et de l'insertion des
   * coordonnées dans les champs.
   *
   * @param  {object} oParams Paramètres :
   *                          	- eParent : calque parent
   *                          	- eModal : calque courant
   *                          	- oGeolocalisation : objet de la classe
   *
   * @return {void}
   */
  this.vChargeEvenementClicEnregistrementGeolocalisation = function(oParams) {
    oParams.eModal
      .find(".btn_enregistre_geolocalisation")
      .off("click")
      .on("click", function(oEvent) {
        oEvent.preventDefault();

        // Au clic sur le bouton d'enregistrement
        // on stocke les valeurs dans le calque
        // d'édition (parent) et on ferme le calque
        // de géolocalisation.

        // Select ajax de la ville.
        var eSelect = oParams.eModal.find(".select-ville");
        var oCommune = oParams.oGeolocalisation.oDetailCommuneDepuisLibelle(
          eSelect
        );

        // Insertion de l'adresse dans le formulaire de géolocalisation.
        oParams.eParent
          .find(".sAdresse")
          .val(oParams.eModal.find(".sAdresse").val());
        oParams.eParent
          .find(".fLatitude")
          .val(oParams.eModal.find(".fLatitude").val());
        oParams.eParent
          .find(".fLongitude")
          .val(oParams.eModal.find(".fLongitude").val());

        // Insertion d'une option sélectionnée par défaut
        // correspondant à la fille sélectionnée.
        oParams.eParent
          .find(".select-ville")
          .append(
            '<option value="' +
              oCommune.nIdCommune +
              '" selected>' +
              oCommune.sCodePostal +
              " " +
              oCommune.sVille +
              "</option>"
          );

        // Fermeture du calque de géolocalisation.
        vFermeCalque("calque_geolocalisation_adresse");
      });
  };

  /**
   * Callback appelée une fois le HTML de la liste des
   * logs récupérée puis insérée.
   *
   * @param  {object} oParams Paramètres contenant l'id du zombie et la modale.
   *
   * @return {void}
   */
  this.vDynamiseWidgetListeLogs = function(oParams) {
    // On ajoute l'id du zombie en premier
    // paramètre de la route de la liste.
    oParams.eModal.find(".liste_logs").removeClass("variable_1_0");
    oParams.eModal
      .find(".liste_logs")
      .addClass("variable_1_" + oParams.nIdZombie);

    // On lance le chargement de la liste.
    this.vChargeListe("", oParams.eModal.find(".liste_logs"));
  };
  /**
   * Dynamisation de l'édition d'un élément.
   *
   * @param  {object} oReponseJSON Informations au format JSON.
   * @param  {object} oParams      Infos concernant l'action lancée.
   *
   * @return {void}
   */
  this.vDynamisationRecrutementJSON = function(oReponseJSON, oParams) {
    // Id de l'élément à éditer.
    var nIdElement = 0;
    if (
      typeof oReponseJSON.oElement != "undefined" &&
      typeof oReponseJSON.oElement.nIdElement != "undefined"
    ) {
      nIdElement = oReponseJSON.oElement.nIdElement;
    }

    var sIdentifiantCalque = "modal_calque_recrutement_zombie";

    var oModal = new Modal(sIdentifiantCalque);

    // Stockage de l'élément du DOM représentant le formulaire
    // de notre calque afin de préciser ce que le coeur
    // doit dynamiser.
    oParams.eFormulaire = oModal.eModal.find("form");

    // Demande au coeur de dynamiser le formulaire avec les
    // informations récupérées grâce à notre action.
    this.vChargeFormulaireData(oReponseJSON, oParams);

    // On ajoute l'identifiant de l'élément sur le bouton
    // afin que l'action et la route aient cette info
    // pour la transmettre au contrôleur d'action.
    oModal.eModal
      .find(".action_zombie_btn_enregistre_recrutement_zombie")
      .addClass("variable_1_" + nIdElement);

    // L'élément du DOM représentant notre select.
    var eSelect = oModal.eModal.find(".select-section");

    // On appelle la méthode permettant de transformer
    // notre select des sections avec les sections que possède le chef de section connecté.
    this.vTransformeSelectSection(eSelect, oReponseJSON.aIdsSections);

    // Charge le listener sur le bouton de recrutement
    // pour afficher un message de confirmation
    var elBoutonRecrutement = $(".btn_enregistre_recrutement_zombie");
    elBoutonRecrutement.unbind("click");
    elBoutonRecrutement.click(function() {
      var oParamsConfirmation = {};
      oParamsConfirmation.aVariables = [nIdElement];
      oParamsConfirmation.oBouton = elBoutonRecrutement;
      oParamsConfirmation.sLibelleBoutonContinuer = "Recruter";
      var oCallbackConfirmation = oThis.oGetFonctionCallback(
        elBoutonRecrutement,
        function() {
          if ($("#modal_calque_recrutement_zombie .select-section").val() > 0) {
            oThis.vExecuteAction(
              "",
              "zombie",
              "btn_enregistre_recrutement_zombie",
              oParamsConfirmation
            );
          }
        },
        oParamsConfirmation
      );
      oThis.vAlerteConfirmation(
        "Êtes-vous sur de vouloir recruter ce zombie moisi ?",
        oCallbackConfirmation,
        "",
        "",
        "",
        oParamsConfirmation
      );
    });

    // Ouverture et stockage de l'instance de calque.
    oModal.oOpenModal();
    aInstancesCalques[sIdentifiantCalque] = oModal;
  };

  /**
   * Transformation du select de section en Select2
   * permettant de rechercher via appel Ajax.
   *
   * @param  {element} eSelect Elément du DOM représentant le select visé.
   *
   * @return {void}
   */
  this.vTransformeSelectSection = function(eSelect, aIdsSections) {
    var aChamps = ["id_section", "libelle"];
    var sRestrictionIdsSections =
      "AND id_section IN (" + aIdsSections.join(",") + ")";

    // Les paramètres du select.
    // Les lignes commentées sont des lignes par défaut dans
    // la méthode aGetSelect2JSONResearch et donc je les
    // laisse juste pour info.
    var oParamsSelect = {
      eSelect2: eSelect, // L'élément du DOM représentant notre select.
      nMinimumLength: 0, // Le nombre minimum de caractères saisis pour lancer la recherche.
      aChamps: aChamps, // On précise quel champ va être dans la value de l'option (1er), et lequel va être dans le label (2nd).
      sTable: "section", // La table visée dans la base.
      sOrderBy: "libelle", // Champ sur lequel trié.
      // bAllowClear: true,                   // On peut ajouter une croix pour supprimer la valeur sélectionnée.
      // sModuleRoute: 'base',                // Le module contenant la route de dynamisation.
      // sRoute: 'json_load_select_research', // La route de dynamisation.
      // sSousMode: '',                       // Un éventuel sous mode dans le cas d'une route personnalisée.
      sRestriction: sRestrictionIdsSections // On peut ajouter un bout de SQL pour compléter le WHERE
    };

    // On instancie le select pour qu'il devienne Select2
    // et accepte la recherche via appel Ajax.
    this.aGetSelect2JSONResearch(oParamsSelect);
  };

  /**
   * Recharge le calque de consultation du zombie et ferme le calque de recrutement
   *
   * @param  {object} oReponseJSON Informations au format JSON.
   * @param  {object} oParams      Infos concernant l'action lancée.
   *
   * @return {void}
   */
  this.vFermeRecrutementZombie = function(oReponseJSON, oParams) {
    // Récupère l'identifiant du zombie
    nIdElement = oReponseJSON.oElement.nIdElement;

    // Les paramètres à passer à la route (l'id de l'élément).
    var oParamsConsultation = {
      aVariables: [nIdElement]
    };
    // Exécution de l'action présente dans le modulé zombie
    // en lui passant nos paramètres définis ci-dessus.
    this.vExecuteAction(
      "",
      "zombie",
      "btn_ouverture_consultation_zombie",
      oParamsConsultation
    );
    vFermeCalque("calque_recrutement_zombie");
  };

  /**
   * Dynamisation trombinoscope
   *
   * @param  {object} oReponseJSON INFOS Json
   * @param  {object} oParams      Infos concernant l'action lancée
   *
   * @return {void}
   */
  this.vDynamisationTrombinoscopeJSON = function(oReponseJSON, oParams) {
    var sIdentifiantCalque = "modal_calque_trombinoscope_zombie";
    var oModal = new Modal(sIdentifiantCalque);

    // Bloc cible et clone initial
    eBlocCible = oModal.eModal.find(".grid");
    eCloneInitial = eBlocCible.find(".clone");

    var aElements = oReponseJSON.aElements;

    $.each(oReponseJSON.aElements, function(nIndex, oElement) {
      var eClone = eCloneInitial.clone();
      eClone.removeClass("clone");
      $.each(oElement, function(sNomChamp, sValeur) {
        // On insère la valeur dans les éléments correspondants.
        eClone.find("." + sNomChamp).html(sValeur);
        eBlocCible.append(eClone);
      });
      eClone.find(".sUrlAvatar").attr("src", oElement.sUrlAvatar);
    });

    // Ouverture et stockage de l'instance de calque.
    oModal.oOpenModal();
    aInstancesCalques[sIdentifiantCalque] = oModal;
  };
}
