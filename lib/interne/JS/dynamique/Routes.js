var oUtiles = new Utiles();
if (typeof oParametres == 'undefined') {
    var oParametres = oUtiles.oGetParamsFichierJSBaliseScript();
}

var szModulePage = oParametres.szModule;
var szZoneCourante = oParametres.szZone;

var szModeCourant = oParametres.szMode;

var szControllerCourant = oParametres.szController.replace('HTML', '');

var szTypeProjet = 'application';

var szModeRecherche = 'listener';

var bVersionProd = false;

var oTemplates = {};
oTemplates.admin = 'admin-fullscreen';
oTemplates.application = 'admin-fullscreen';
oTemplates.site = 'site';

var bCloisonnementModules = true;

var aExclusionsCloisonnement = {};
aExclusionsCloisonnement['all'] = ['menu', 'internationalisation', 'chat'];


var bZoneAdminUniquement = true;

var szUrlBase = 'http://dawn-of-the-dead.doing-net.com:8080/';

if (typeof oParametres.szZone != 'undefined') {
     szZoneCourante = oParametres.szZone;
}
/**
* Génération et écriture du fichier JS contenant les routes et les blocs.
*
* @return void
*/
function vChargeClassesModules()
{
    if (szZoneCourante == 'site') {
        if (typeof DefautPublic != 'undefined') {
            var oDefautPublic = new DefautPublic();
            if (typeof oDefautPublic.vInit != 'undefined') {
                oDefautPublic.vInit();
            } else if (typeof Defaut != 'undefined' && szModulePage == 'defaut') {
                var oDefaut = new Defaut();
                oDefaut.vInit();
            } 
        } else if (typeof Defaut != 'undefined' && szModulePage == 'defaut') {
            var oDefaut = new Defaut();
            oDefaut.vInit();
        } 
    } else {
        if (typeof Defaut != 'undefined' && szModulePage == 'defaut') {
            var oDefaut = new Defaut();
            oDefaut.vInit();
        }

    }
    if (szZoneCourante == 'site') {
        if (typeof DocumentPublic != 'undefined') {
            var oDocumentPublic = new DocumentPublic();
            if (typeof oDocumentPublic.vInit != 'undefined') {
                oDocumentPublic.vInit();
            } else if (typeof Document != 'undefined' && szModulePage == 'document') {
                var oDocument = new Document();
                oDocument.vInit();
            } 
        } else if (typeof Document != 'undefined' && szModulePage == 'document') {
            var oDocument = new Document();
            oDocument.vInit();
        } 
    } else {
        if (typeof Document != 'undefined' && szModulePage == 'document') {
            var oDocument = new Document();
            oDocument.vInit();
        }

    }
    if (szZoneCourante == 'site') {
        if (typeof Easy2doPublic != 'undefined') {
            var oEasy2doPublic = new Easy2doPublic();
            if (typeof oEasy2doPublic.vInit != 'undefined') {
                oEasy2doPublic.vInit();
            } else if (typeof Easy2do != 'undefined' && szModulePage == 'easy2do') {
                var oEasy2do = new Easy2do();
                oEasy2do.vInit();
            } 
        } else if (typeof Easy2do != 'undefined' && szModulePage == 'easy2do') {
            var oEasy2do = new Easy2do();
            oEasy2do.vInit();
        } 
    } else {
        if (typeof Easy2do != 'undefined' && szModulePage == 'easy2do') {
            var oEasy2do = new Easy2do();
            oEasy2do.vInit();
        }

    }
    if (szZoneCourante == 'site') {
        if (typeof AuthentificationPublic != 'undefined') {
            var oAuthentificationPublic = new AuthentificationPublic();
            if (typeof oAuthentificationPublic.vInit != 'undefined') {
                oAuthentificationPublic.vInit();
            } else if (typeof Authentification != 'undefined' && szModulePage == 'authentification') {
                var oAuthentification = new Authentification();
                oAuthentification.vInit();
            } 
        } else if (typeof Authentification != 'undefined' && szModulePage == 'authentification') {
            var oAuthentification = new Authentification();
            oAuthentification.vInit();
        } 
    } else {
        if (typeof Authentification != 'undefined' && szModulePage == 'authentification') {
            var oAuthentification = new Authentification();
            oAuthentification.vInit();
        }

    }
    if (szZoneCourante == 'site') {
        if (typeof BasePublic != 'undefined') {
            var oBasePublic = new BasePublic();
            if (typeof oBasePublic.vInit != 'undefined') {
                oBasePublic.vInit();
            } else if (typeof Base != 'undefined' && szModulePage == 'base') {
                var oBase = new Base();
                oBase.vInit();
            } 
        } else if (typeof Base != 'undefined' && szModulePage == 'base') {
            var oBase = new Base();
            oBase.vInit();
        } 
    } else {
        if (typeof Base != 'undefined' && szModulePage == 'base') {
            var oBase = new Base();
            oBase.vInit();
        }

    }
    if (szZoneCourante == 'site') {
        if (typeof GeolocalisationPublic != 'undefined') {
            var oGeolocalisationPublic = new GeolocalisationPublic();
            if (typeof oGeolocalisationPublic.vInit != 'undefined') {
                oGeolocalisationPublic.vInit();
            } else if (typeof Geolocalisation != 'undefined' && szModulePage == 'geolocalisation') {
                var oGeolocalisation = new Geolocalisation();
                oGeolocalisation.vInit();
            } 
        } else if (typeof Geolocalisation != 'undefined' && szModulePage == 'geolocalisation') {
            var oGeolocalisation = new Geolocalisation();
            oGeolocalisation.vInit();
        } 
    } else {
        if (typeof Geolocalisation != 'undefined' && szModulePage == 'geolocalisation') {
            var oGeolocalisation = new Geolocalisation();
            oGeolocalisation.vInit();
        }

    }
    if (szZoneCourante == 'site') {
        if (typeof GroupePublic != 'undefined') {
            var oGroupePublic = new GroupePublic();
            if (typeof oGroupePublic.vInit != 'undefined') {
                oGroupePublic.vInit();
            } else if (typeof Groupe != 'undefined' && szModulePage == 'groupe') {
                var oGroupe = new Groupe();
                oGroupe.vInit();
            } 
        } else if (typeof Groupe != 'undefined' && szModulePage == 'groupe') {
            var oGroupe = new Groupe();
            oGroupe.vInit();
        } 
    } else {
        if (typeof Groupe != 'undefined' && szModulePage == 'groupe') {
            var oGroupe = new Groupe();
            oGroupe.vInit();
        }

    }
    if (szZoneCourante == 'site') {
        if (typeof LogsPublic != 'undefined') {
            var oLogsPublic = new LogsPublic();
            if (typeof oLogsPublic.vInit != 'undefined') {
                oLogsPublic.vInit();
            } else if (typeof Logs != 'undefined' && szModulePage == 'logs') {
                var oLogs = new Logs();
                oLogs.vInit();
            } 
        } else if (typeof Logs != 'undefined' && szModulePage == 'logs') {
            var oLogs = new Logs();
            oLogs.vInit();
        } 
    } else {
        if (typeof Logs != 'undefined' && szModulePage == 'logs') {
            var oLogs = new Logs();
            oLogs.vInit();
        }

    }
    if (szZoneCourante == 'site') {
        if (typeof MenuPublic != 'undefined') {
            var oMenuPublic = new MenuPublic();
            if (typeof oMenuPublic.vInit != 'undefined') {
                oMenuPublic.vInit();
            } else if (typeof Menu != 'undefined' && szModulePage == 'menu') {
                var oMenu = new Menu();
                oMenu.vInit();
            } 
        } else if (typeof Menu != 'undefined' && szModulePage == 'menu') {
            var oMenu = new Menu();
            oMenu.vInit();
        } 
    } else {
        if (typeof Menu != 'undefined' && szModulePage == 'menu') {
            var oMenu = new Menu();
            oMenu.vInit();
        }

    }
    if (szZoneCourante == 'site') {
        if (typeof HumainPublic != 'undefined') {
            var oHumainPublic = new HumainPublic();
            if (typeof oHumainPublic.vInit != 'undefined') {
                oHumainPublic.vInit();
            } else if (typeof Humain != 'undefined' && szModulePage == 'humain') {
                var oHumain = new Humain();
                oHumain.vInit();
            } 
        } else if (typeof Humain != 'undefined' && szModulePage == 'humain') {
            var oHumain = new Humain();
            oHumain.vInit();
        } 
    } else {
        if (typeof Humain != 'undefined' && szModulePage == 'humain') {
            var oHumain = new Humain();
            oHumain.vInit();
        }

    }
    if (szZoneCourante == 'site') {
        if (typeof ZombiePublic != 'undefined') {
            var oZombiePublic = new ZombiePublic();
            if (typeof oZombiePublic.vInit != 'undefined') {
                oZombiePublic.vInit();
            } else if (typeof Zombie != 'undefined' && szModulePage == 'zombie') {
                var oZombie = new Zombie();
                oZombie.vInit();
            } 
        } else if (typeof Zombie != 'undefined' && szModulePage == 'zombie') {
            var oZombie = new Zombie();
            oZombie.vInit();
        } 
    } else {
        if (typeof Zombie != 'undefined' && szModulePage == 'zombie') {
            var oZombie = new Zombie();
            oZombie.vInit();
        }

    }
}


var aRoutes = [];
aRoutes['defaut'] = [];
aRoutes['document'] = [];
aRoutes['easy2do'] = [];
aRoutes['easy2do']['html_sauvegarde_recherche'] = '/admin/easy2do/recherche/sauvegarde-recherche.html';
aRoutes['easy2do']['json_sauvegarde_recherche'] = '/admin/easy2do/recherche/sauvegarde-recherche.json';
aRoutes['authentification'] = [];
aRoutes['authentification']['login'] = '/admin/login';
aRoutes['authentification']['connexion'] = '/admin/authentification/connexion';
aRoutes['authentification']['deconnexion'] = '/admin/authentification/deconnexion';
aRoutes['authentification']['charge_liste_ressources'] = '/admin/droits/charge';
aRoutes['authentification']['html_form_connexion'] = '/utilisateur/formulaire-connexion.json';
aRoutes['authentification']['json_substitution_utilisateur'] = '/admin/authentification/[nIdElement]/substitution.json';
aRoutes['authentification']['json_fin_substitution'] = '/admin/authentification/fin-substitution.json';
aRoutes['authentification']['html_form_changer_mot_de_passe'] = '/public/authentification/[nIdElement]/[sJeton]/form-changer-mot-de-passe.html';
aRoutes['authentification']['json_verifier_utilisateur_connecte'] = '/admin/authentification/verifier-utilisateur-connecte.json';
aRoutes['authentification']['json_changer_mot_de_passe'] = '/public/authentification/changer-mot-de-passe.json';
aRoutes['authentification']['html_signup'] = '/admin/authentification/signup.html';
aRoutes['authentification']['json_envoi_mail_signup'] = '/admin/authentification/initialisation-mot-de-passe.json';
aRoutes['authentification']['html_creation_password'] = '/admin/authentification/[nIdElement]/[sJeton]/formulaire-creation-mot-passe.html';
aRoutes['authentification']['json_apres_ouverture_signup'] = '/admin/authentification/apres-ouverture-signup.json';
aRoutes['authentification']['html_mot_passe_oublie'] = '/admin/authentification/mot-passe-oubli.html';
aRoutes['authentification']['json_envoi_mail_reinitialisation_password'] = '/admin/authentification/envoi-mail-reinitialisation-mot-de-passe.json';
aRoutes['base'] = [];
aRoutes['base']['json_details_bdd'] = '/base/details-base-donnees.json';
aRoutes['base']['json_load_select_research'] = '/admin/base/base/select-research-request.json';
aRoutes['geolocalisation'] = [];
aRoutes['geolocalisation']['html_geolocalisation_adresse'] = '/admin/geolocalisation/geolocalisation-adresse.html';
aRoutes['geolocalisation']['json_geolocalisation_adresse'] = '/admin/geolocalisation/geolocalisation-adresse.json';
aRoutes['groupe'] = [];
aRoutes['groupe']['html_accueil_groupe'] = '/admin/groupe/accueil-groupes.html';
aRoutes['groupe']['html_liste_groupe'] = '/admin/groupe/liste.html';
aRoutes['groupe']['html_edition_groupe'] = '/admin/groupe/edition.html';
aRoutes['groupe']['html_filtre_groupe'] = '/admin/groupe/filtre.html';
aRoutes['groupe']['json_recherche_groupe'] = '/admin/groupe/[nIdElement]/recherche.json';
aRoutes['groupe']['json_dynamisation_groupe'] = '/admin/groupe/[nIdElement]/dynamisation-edition.json';
aRoutes['groupe']['json_save_groupe'] = '/admin/groupe/enregister.json';
aRoutes['groupe']['json_supprimer_groupe'] = '/admin/groupe/[nIdElement]/supprimer.json';
aRoutes['logs'] = [];
aRoutes['logs']['html_accueil_logs'] = '/admin/logs/logs/accueil.html';
aRoutes['logs']['json_recherche_logs'] = '/admin/logs/logs/[nIdElement]/[sFiltreType]/recherche.json';
aRoutes['logs']['json_load_liste'] = '/admin/logs/logs/[nIdElement]/[sFiltreType]/load-liste.json';
aRoutes['logs']['html_consultation_logs'] = '/admin/logs/logs/consultation.html';
aRoutes['logs']['json_dynamisation_consultation_logs'] = '/admin/logs/logs/[nIdElement]/dynamisation-consultation.json';
aRoutes['logs']['html_liste_logs'] = '/admin/logs/logs/liste.html';
aRoutes['menu'] = [];
aRoutes['menu']['get_menu'] = '/admin/menu.html';
aRoutes['humain'] = [];
aRoutes['humain']['html_accueil_humain'] = '/admin/humain/accueil-humain.html';
aRoutes['humain']['json_recherche_humain'] = '/admin/humain/recherche.json';
aRoutes['humain']['json_dynamisation_edition_humain'] = '/admin/humain/[nIdElement]/dynamisation-edition.json';
aRoutes['humain']['html_edition_humain'] = '/admin/humain/edition-humain.html';
aRoutes['humain']['json_enregistre_edition_humain'] = '/admin/humain/[nIdElement]/enregistre-edition.json';
aRoutes['humain']['json_dynamisation_consultation_humain'] = '/admin/humain/[nIdElement]/dynamisation-consultation.json';
aRoutes['humain']['html_consultation_humain'] = '/admin/humain/consultation-humain.html';
aRoutes['humain']['json_suppression_humain'] = '/admin/humain/[nIdElement]/suppression-humain.json';
aRoutes['humain']['json_upload_avatar'] = '/admin/humain/[nIdElement]/upload-avatar.json';
aRoutes['humain']['html_recuperation_avatar'] = '/document/[sModule]/[sSousDossier]/[sNomFichier]/[sType]';
aRoutes['humain']['json_export_csv'] = '/admin/humain/export-humains.json';
aRoutes['zombie'] = [];
aRoutes['zombie']['html_accueil_zombie'] = '/admin/zombie/[bMesSoldats]/accueil-zombie.html';
aRoutes['zombie']['json_recherche_zombie'] = '/admin/zombie/recherche.json';
aRoutes['zombie']['json_dynamisation_edition_zombie'] = '/admin/zombie/[nIdElement]/dynamisation-edition.json';
aRoutes['zombie']['html_edition_zombie'] = '/admin/zombie/edition-zombie.html';
aRoutes['zombie']['json_enregistre_edition_zombie'] = '/admin/zombie/[nIdElement]/enregistre-edition.json';
aRoutes['zombie']['json_dynamisation_consultation_zombie'] = '/admin/zombie/[nIdElement]/dynamisation-consultation.json';
aRoutes['zombie']['html_consultation_zombie'] = '/admin/zombie/consultation-zombie.html';
aRoutes['zombie']['json_suppression_zombie'] = '/admin/zombie/[nIdElement]/suppression-zombie.json';
aRoutes['zombie']['json_upload_avatar'] = '/admin/zombie/[nIdElement]/upload-avatar.json';
aRoutes['zombie']['html_recuperation_avatar'] = '/document/[sModule]/[sSousDossier]/[sNomFichier]/[sType]';
aRoutes['zombie']['json_dynamisation_recherche'] = '/admin/zombie/dynamisation-recherche.json';
aRoutes['zombie']['json_export_csv'] = '/admin/zombie/export-zombies.json';
aRoutes['zombie']['html_accueil_section'] = '/admin/zombie/accueil-section.html';
aRoutes['zombie']['json_recherche_section'] = '/admin/section/recherche.json';
aRoutes['zombie']['json_dynamisation_edition_section'] = '/admin/section/[nIdElement]/dynamisation-edition.json';
aRoutes['zombie']['html_edition_section'] = '/admin/section/edition-section.html';
aRoutes['zombie']['json_enregistre_edition_section'] = '/admin/section/[nIdElement]/enregistre-edition.json';
aRoutes['zombie']['json_dynamisation_consultation_section'] = '/admin/section/[nIdElement]/dynamisation-consultation.json';
aRoutes['zombie']['html_consultation_section'] = '/admin/section/consultation-section.html';
aRoutes['zombie']['json_suppression_section'] = '/admin/section/[nIdElement]/suppression-section.json';
aRoutes['zombie']['json_dynamisation_recrutement_zombie'] = '/admin/zombie/[nIdElement]/recrutement-zombie.json';
aRoutes['zombie']['html_recrutement_zombie'] = '/admin/zombie/recrutement-zombie.html';
aRoutes['zombie']['json_enregistre_recrutement_zombie'] = '/admin/zombie/[nIdElement]/enregistre-zombie.json';
aRoutes['zombie']['html_accueil_soldat'] = '/admin/zombie/[bMesSoldats]/accueil-soldat.html';
aRoutes['zombie']['json_dynamisation_trombinoscope_zombie'] = '/admin/zombie/dynamisation-trombinoscope-zombie.json';
aRoutes['zombie']['html_trombinoscope_zombie'] = '/admin/zombie/trombinoscope-zombie.html';

var aBlocs = [];

var objBloc = new Object();
objBloc.szRoute = 'html_sauvegarde_recherche';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin'];
aBlocs['easy2do'] = [];
aBlocs['easy2do']['calque_sauvegarde_recherche'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_form_connexion';
objBloc.nPriorite = 2;
objBloc.nOrdre = 3;
objBloc.aZonesAffichage = ['application', 'admin'];
aBlocs['authentification'] = [];
aBlocs['authentification']['calque_formulaire_connexion'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_signup';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin'];
aBlocs['authentification']['calque_signup'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_mot_passe_oublie';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin'];
aBlocs['authentification']['calque_mot_passe_oublie'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_geolocalisation_adresse';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin', 'application'];
aBlocs['geolocalisation'] = [];
aBlocs['geolocalisation']['calque_geolocalisation_adresse'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_edition_groupe';
objBloc.nPriorite = 2;
objBloc.nOrdre = 2;
objBloc.aZonesAffichage = ['admin', 'application'];
aBlocs['groupe'] = [];
aBlocs['groupe']['calque_edition_groupe'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_liste_logs';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin'];
aBlocs['logs'] = [];
aBlocs['logs']['widget_liste_logs'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'get_menu';
objBloc.szCallback = 'vChargeEvenementsMenu';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin', 'application'];
aBlocs['menu'] = [];
aBlocs['menu']['menu_lateral'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'get_menu';
objBloc.szCallback = 'vChargeEvenementsMenu';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin', 'application'];
aBlocs['menu']['menu_gauche'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_edition_humain';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin'];
aBlocs['humain'] = [];
aBlocs['humain']['calque_edition_humain'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_consultation_humain';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin'];
aBlocs['humain']['calque_consultation_humain'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_edition_zombie';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin'];
aBlocs['zombie'] = [];
aBlocs['zombie']['calque_edition_zombie'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_consultation_zombie';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin'];
aBlocs['zombie']['calque_consultation_zombie'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_edition_section';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin'];
aBlocs['zombie']['calque_edition_section'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_consultation_section';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin'];
aBlocs['zombie']['calque_consultation_section'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_recrutement_zombie';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin'];
aBlocs['zombie']['calque_recrutement_zombie'] = objBloc;

var objBloc = new Object();
objBloc.szRoute = 'html_trombinoscope_zombie';
objBloc.nPriorite = 1;
objBloc.nOrdre = 1;
objBloc.aZonesAffichage = ['admin'];
aBlocs['zombie']['calque_trombinoscope_zombie'] = objBloc;

var aActions = [];

var oAction = new Object();
oAction.szModule = 'easy2do';
oAction.szRoute = 'json_sauvegarde_recherche';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'formulaire_sauvegarde_recherche';
oAction.szModuleCallback = 'recherche';
oAction.szCallback = 'vFermeCalqueSauvegardeRecherche';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['easy2do'] = [];
aActions['easy2do']['btn_sauvegarde_recherche'] = oAction;

var oAction = new Object();
oAction.szModule = 'authentification';
oAction.szRoute = 'deconnexion';
oAction.szType = 'save_data';
oAction.szModuleCallback = 'authentification';
oAction.szCallback = 'vCallbackDeconnexion';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['authentification'] = [];
aActions['authentification']['btn_deconnexion'] = oAction;

var oAction = new Object();
oAction.szModule = 'authentification';
oAction.szRoute = 'connexion';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'formulaire_login';
oAction.szModuleCallback = 'authentification';
oAction.szCallback = 'vCallbackConnexion';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['authentification']['btn_connexion'] = oAction;

var oAction = new Object();
oAction.szModule = 'authentification';
oAction.szRoute = 'connexion';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'formulaire_login_calque';
oAction.szModuleCallback = 'authentification';
oAction.szCallback = 'vCallbackConnexionCalque';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['authentification']['btn_connexion_calque'] = oAction;

var oAction = new Object();
oAction.szModule = 'authentification';
oAction.szRoute = 'json_substitution_utilisateur';
oAction.szType = 'save_data';
oAction.szModuleCallback = 'authentification';
oAction.szCallback = 'vRedirigeVersAccueil';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['authentification']['btn_substitution_utilisateur'] = oAction;

var oAction = new Object();
oAction.szModule = 'authentification';
oAction.szRoute = 'json_fin_substitution';
oAction.szType = 'save_data';
oAction.szModuleCallback = 'authentification';
oAction.szCallback = 'vRedirigeVersAccueil';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['authentification']['btn_fin_substitution'] = oAction;

var oAction = new Object();
oAction.szModule = 'authentification';
oAction.szRoute = 'json_verifier_utilisateur_connecte';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'authentification';
oAction.szCallback = 'vOuvreModifMotDePasse';
oAction.bAlerte = false;
oAction.aMiddleware = {
            vChargeVueHTML: {
            aParams: {
            sId: 'calque_changer_mot_de_passe',
            sCible: 'bloc'            
            }
            
            }

};
aActions['authentification']['btn_ouvrir_modif_mot_de_passe'] = oAction;

var oAction = new Object();
oAction.szModule = 'authentification';
oAction.szRoute = 'json_changer_mot_de_passe';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'form_changer_mot_de_passe';
oAction.szModuleCallback = 'authentification';
oAction.szCallback = 'vCallbackChangerMotDePasse';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['authentification']['btn_changer_mot_de_passe'] = oAction;

var oAction = new Object();
oAction.szModule = 'authentification';
oAction.szRoute = 'json_envoi_mail_signup';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'formulaire_signup';
oAction.szModuleCallback = 'authentification';
oAction.szCallback = 'vFermeCalqueInitPassword';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['authentification']['btn_envoi_mail_conf_password'] = oAction;

var oAction = new Object();
oAction.szModule = 'authentification';
oAction.szRoute = 'json_apres_ouverture_signup';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'authentification';
oAction.szCallback = 'vApresOuvertureSignup';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['authentification']['btn_apres_ouverture_signup'] = oAction;

var oAction = new Object();
oAction.szModule = 'authentification';
oAction.szRoute = 'json_envoi_mail_reinitialisation_password';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'formulaire_mot_passe_oublie';
oAction.szModuleCallback = 'authentification';
oAction.szCallback = 'vFermeCalqueReinitPassword';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['authentification']['btn_envoi_mail_reinitialisation_password'] = oAction;

var oAction = new Object();
oAction.szModule = 'base';
oAction.szRoute = 'json_details_bdd';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'base';
oAction.szCallback = 'vDynamiseDetailsBdd';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['base'] = [];
aActions['base']['btn_details_bdd'] = oAction;

var oAction = new Object();
oAction.szModule = 'geolocalisation';
oAction.szRoute = 'json_geolocalisation_adresse';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'geolocalisation';
oAction.szCallback = 'vDynamiseCalqueGeoloc';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['geolocalisation'] = [];
aActions['geolocalisation']['btn_ouverture_geolocalisation'] = oAction;

var oAction = new Object();
oAction.szModule = 'geolocalisation';
oAction.szRoute = 'json_geolocalisation_adresse';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'formulaire_geolocalisation_adresse';
oAction.szModuleCallback = 'geolocalisation';
oAction.szCallback = 'vCallbackGeolocalisationAdresse';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['geolocalisation']['btn_geolocalisation_adresse'] = oAction;

var oAction = new Object();
oAction.szModule = 'groupe';
oAction.szRoute = 'html_accueil_groupe';
oAction.szType = 'load_html';
oAction.aMiddleware = {
};
aActions['groupe'] = [];
aActions['groupe']['btn_accueil_groupe'] = oAction;

var oAction = new Object();
oAction.szModule = 'groupe';
oAction.szRoute = 'html_liste_groupe';
oAction.szType = 'load_html';
oAction.szModuleCallback = 'groupe';
oAction.szCallback = 'vInsereListeGroupes';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['groupe']['btn_liste_groupes'] = oAction;

var oAction = new Object();
oAction.szModule = 'groupe';
oAction.szRoute = 'json_dynamisation_groupe';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'groupe';
oAction.szCallback = 'vDynamiseEditionGroupe';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['groupe']['btn_form_edition'] = oAction;

var oAction = new Object();
oAction.szModule = 'groupe';
oAction.szRoute = 'json_supprimer_groupe';
oAction.szType = 'save_data';
oAction.szModuleCallback = 'groupe';
oAction.szCallback = 'vCallBackSupprimerGroupe';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['groupe']['btn_supprimer_groupe'] = oAction;

var oAction = new Object();
oAction.szModule = 'groupe';
oAction.szRoute = 'json_save_groupe';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'formulaire_edition_groupe';
oAction.szModuleCallback = 'groupe';
oAction.szCallback = 'vCallBackSaveGroupe';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['groupe']['btn_save_groupe'] = oAction;

var oAction = new Object();
oAction.szModule = 'groupe';
oAction.szRoute = 'html_filtre_groupe';
oAction.szType = 'load_html';
oAction.szModuleCallback = 'groupe';
oAction.szCallback = 'vInsereFiltreHTML';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['groupe']['btn_filtre_groupe'] = oAction;

var oAction = new Object();
oAction.szModule = 'logs';
oAction.szRoute = 'json_dynamisation_consultation_logs';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'logs';
oAction.szCallback = 'vDynamisationConsultationJSON';
oAction.bAlerte = false;
oAction.aMiddleware = {
            vChargeVueHTML: {
            aParams: {
            sId: 'calque_consultation_logs',
            sCible: 'bloc'            
            }
            
            }

};
aActions['logs'] = [];
aActions['logs']['btn_ouverture_consultation_logs'] = oAction;

var oAction = new Object();
oAction.szModule = 'logs';
oAction.szRoute = 'json_load_liste';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'logs';
oAction.szCallback = 'vCallbackAffichageLogs';
oAction.bAlerte = false;
oAction.aMiddleware = {
            vChargeVueHTML: {
            aParams: {
            sId: 'zone_liste_logs',
            sCible: 'bloc'            
            }
            
            }

};
aActions['logs']['btn_affichage_logs'] = oAction;

var oAction = new Object();
oAction.szModule = 'humain';
oAction.szRoute = 'json_dynamisation_edition_humain';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'humain';
oAction.szCallback = 'vDynamisationEditionJSON';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['humain'] = [];
aActions['humain']['btn_ouverture_edition_humain'] = oAction;

var oAction = new Object();
oAction.szModule = 'humain';
oAction.szRoute = 'json_enregistre_edition_humain';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'formulaire_edition_humain';
oAction.szModuleCallback = 'humain';
oAction.szCallback = 'vUploadSiBesoin';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['humain']['btn_enregistre_edition_humain'] = oAction;

var oAction = new Object();
oAction.szModule = 'humain';
oAction.szRoute = 'json_dynamisation_consultation_humain';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'humain';
oAction.szCallback = 'vDynamisationConsultationJSON';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['humain']['btn_ouverture_consultation_humain'] = oAction;

var oAction = new Object();
oAction.szModule = 'humain';
oAction.szRoute = 'json_suppression_humain';
oAction.szType = 'save_data';
oAction.szModuleCallback = 'humain';
oAction.szCallback = 'vFermeConsultationEtRefreshListe';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['humain']['btn_suppression_humain'] = oAction;

var oAction = new Object();
oAction.szModule = 'zombie';
oAction.szRoute = 'json_dynamisation_edition_zombie';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'zombie';
oAction.szCallback = 'vDynamisationEditionJSON';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['zombie'] = [];
aActions['zombie']['btn_ouverture_edition_zombie'] = oAction;

var oAction = new Object();
oAction.szModule = 'zombie';
oAction.szRoute = 'json_enregistre_edition_zombie';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'formulaire_edition_zombie';
oAction.szModuleCallback = 'zombie';
oAction.szCallback = 'vUploadSiBesoin';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['zombie']['btn_enregistre_edition_zombie'] = oAction;

var oAction = new Object();
oAction.szModule = 'zombie';
oAction.szRoute = 'json_dynamisation_consultation_zombie';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'zombie';
oAction.szCallback = 'vDynamisationConsultationJSON';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['zombie']['btn_ouverture_consultation_zombie'] = oAction;

var oAction = new Object();
oAction.szModule = 'zombie';
oAction.szRoute = 'json_suppression_zombie';
oAction.szType = 'save_data';
oAction.szModuleCallback = 'zombie';
oAction.szCallback = 'vFermeConsultationEtRefreshListe';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['zombie']['btn_suppression_zombie'] = oAction;

var oAction = new Object();
oAction.szModule = 'zombie';
oAction.szRoute = 'json_dynamisation_recherche';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'zombie';
oAction.szCallback = 'vDynamisationFormulaireRecherche';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['zombie']['btn_dynamisation_recherche'] = oAction;

var oAction = new Object();
oAction.szModule = 'zombie';
oAction.szRoute = 'json_dynamisation_edition_section';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'section';
oAction.szCallback = 'vDynamisationEditionJSON';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['zombie']['btn_ouverture_edition_section'] = oAction;

var oAction = new Object();
oAction.szModule = 'zombie';
oAction.szRoute = 'json_enregistre_edition_section';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'formulaire_edition_section';
oAction.szModuleCallback = 'section';
oAction.szCallback = 'vFermeEditionEtRefreshListe';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['zombie']['btn_enregistre_edition_section'] = oAction;

var oAction = new Object();
oAction.szModule = 'zombie';
oAction.szRoute = 'json_dynamisation_consultation_section';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'section';
oAction.szCallback = 'vDynamisationConsultationJSON';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['zombie']['btn_ouverture_consultation_section'] = oAction;

var oAction = new Object();
oAction.szModule = 'zombie';
oAction.szRoute = 'json_suppression_section';
oAction.szType = 'save_data';
oAction.szModuleCallback = 'section';
oAction.szCallback = 'vFermeConsultationEtRefreshListe';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['zombie']['btn_suppression_section'] = oAction;

var oAction = new Object();
oAction.szModule = 'zombie';
oAction.szRoute = 'json_dynamisation_recrutement_zombie';
oAction.szType = 'load_data';
oAction.szModuleCallback = 'zombie';
oAction.szCallback = 'vDynamisationRecrutementJSON';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['zombie']['btn_ouverture_recrutement_zombie'] = oAction;

var oAction = new Object();
oAction.szModule = 'zombie';
oAction.szRoute = 'json_enregistre_recrutement_zombie';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'formulaire_recrutement_zombie';
oAction.szModuleCallback = 'zombie';
oAction.szCallback = 'vFermeRecrutementZombie';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['zombie']['btn_enregistre_recrutement_zombie'] = oAction;

var oAction = new Object();
oAction.szModule = 'zombie';
oAction.szRoute = 'json_dynamisation_trombinoscope_zombie';
oAction.szType = 'save_data';
oAction.szIdFormulaireSave = 'formulaire_recherche_zombie';
oAction.szModuleCallback = 'zombie';
oAction.szCallback = 'vDynamisationTrombinoscopeJSON';
oAction.bAlerte = false;
oAction.aMiddleware = {
};
aActions['zombie']['btn_ouverture_trombinoscope_zombie'] = oAction;
var szSousDossierArbo = '';
var szZone = 'application';
